package com.shotsurf;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class UserDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_detail);
        findViewById(R.id.menuItem).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customMenuItem();
            }
        });
    }

   private void customMenuItem(){
       // custom dialog
       final Dialog dialog = new Dialog(UserDetailActivity.this);
       dialog.setContentView(R.layout.menu_item_dialog);
       // set the custom dialog components - text, image and button
       TextView text = (TextView) dialog.findViewById(R.id.text);
       dialog.show();
   }
}
