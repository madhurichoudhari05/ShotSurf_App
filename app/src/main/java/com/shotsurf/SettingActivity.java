package com.shotsurf;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.wearable.Wearable;
import com.shotsurf.common.AppConstants;
import com.shotsurf.views.CommonUtils;
import com.shotsurf.views.SyncNewActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class SettingActivity extends AppCompatActivity implements  GoogleApiClient.ConnectionCallbacks
        , GoogleApiClient.OnConnectionFailedListener  {

    private TextView tvLogout,tvContact;
    private GoogleApiClient mGoogleApiClient1;
    private Context context;

    private static final String TAG = SettingActivity.class.getSimpleName();
    private static final int REQUEST_CODE_PICK_CONTACTS = 1;
    private Uri uriContact;
    private String contactID;
    private int clickId = -1;
    private static final int auto = 0;
    public static final int ALL_PERMISSION = 99;
    private GoogleApiClient mGoogleApiClient;
    private LoginActivity loginLogout;

    protected  void userResponse(String[] permission){}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        context = SettingActivity.this;
        tvLogout = (TextView) findViewById(R.id.tvLogout);
        tvContact = (TextView) findViewById(R.id.tvContact);

        loginLogout =new LoginActivity();


        tvContact.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {

               // startActivityForResult(new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI), REQUEST_CODE_PICK_CONTACTS);

                startActivity(new Intent(context, SyncNewActivity.class));
            }
        });


        tvLogout.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {


//                LoginActivity .setLogout();


                CommonUtils.savePreferencesBoolean(context, AppConstants.FIRST_TIME_TUTORIAL, false);

                //  Activity.fi
//                if (mGoogleApiClient.isConnected())
//                    Auth.GoogleSignInApi.signOut(mGoogleApiClient);
              //  signOut();







//                mGoogleApiClient1 = new GoogleApiClient.Builder(context)
//                        .addConnectionCallbacks( new GoogleApiClient.ConnectionCallbacks() {
//                            @Override
//                            public void onConnected(Bundle connectionHint) {
//                                Log.i( "", "onConnected: " + connectionHint);
//
//                      /*          Plus.AccountApi.clearDefaultAccount(mGoogleApiClient1);
//                                Plus.AccountApi.revokeAccessAndDisconnect(mGoogleApiClient1);
//                                mGoogleApiClient.disconnect();*/
//
//                                if (mGoogleApiClient.isConnected()) {
//                                    Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
//                                    mGoogleApiClient.disconnect();
//                                    mGoogleApiClient.connect();
//
//                                }
//                                // Now you can use the Data Layer API
//                            }
//                            @Override
//                            public void onConnectionSuspended(int cause) {
//                                Log.i( "", "onConnectionSuspended: " + cause);
//                            }
//                        })
//                        .addOnConnectionFailedListener( new GoogleApiClient.OnConnectionFailedListener() {
//                            @Override
//                            public void onConnectionFailed(ConnectionResult result) {
//                                Log.i( "", "onConnectionFailed: " + result);
//                            }
//                        })
//                        // Request access only to the Wearable API
//                        .addApi(Wearable.API)
//                        .build();

                CommonUtils.savePreferencesBoolean(SettingActivity.this, AppConstants.FIRST_TIME_TUTORIAL, false);
                startActivity(new Intent(context, LoginActivity.class));
                finish();


            }
        });

    }

        @Override
        protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);

            if (requestCode == REQUEST_CODE_PICK_CONTACTS && resultCode == RESULT_OK) {
                Log.d(TAG, "Response: " + data.toString());
                uriContact = data.getData();

                retrieveContactName();
                //retrieveContactNumber();
                // retrieveContactPhoto();

            }
        }


    protected boolean isPerGiven(String per) {
        if (ContextCompat.checkSelfPermission(SettingActivity.this, per)
                == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    private void  permissions() {
        /*location*/
        List permission = new ArrayList();

        if (isPerGiven(Manifest.permission.READ_PHONE_STATE)) {

        } else {
            permission.add(Manifest.permission.READ_PHONE_STATE);
        }

        if (permission.size() > 0) {
            clickId = auto;
            String[] str = new String[permission.size()];
            isPermissionRequired((String[]) permission.toArray(str));
        }
    }

    protected boolean isPermissionRequired(String[] requestPer) {
        List<String> requiredPerm = new ArrayList<>();
        for (String per : requestPer) {
            if (ContextCompat.checkSelfPermission(SettingActivity.this, per) != PackageManager.PERMISSION_GRANTED) {
                requiredPerm.add(per);
            }
        }

        if (requiredPerm.size() > 0) {
            String[] perArray = new String[requiredPerm.size()];
            perArray = requiredPerm.toArray(perArray);
            ActivityCompat.requestPermissions(SettingActivity.this, perArray, ALL_PERMISSION);
        } else {
            return false;
        }
        return true;
    }



    private void retrieveContactNumber() {

        String contactNumber = null;

        // getting contacts ID
        Cursor cursorID = getContentResolver().query(uriContact,
                new String[]{ContactsContract.Contacts._ID},
                null, null, null);

        if (cursorID.moveToFirst()) {

            contactID = cursorID.getString(cursorID.getColumnIndex(ContactsContract.Contacts._ID));
        }

        cursorID.close();

        Log.d(TAG, "Contact ID: " + contactID);

        // Using the contact ID now we will get contact phone number
        Cursor cursorPhone = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER},

                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ? AND " +
                        ContactsContract.CommonDataKinds.Phone.TYPE + " = " +
                        ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE,

                new String[]{contactID},
                null);

        if (cursorPhone.moveToFirst()) {
            contactNumber = cursorPhone.getString(cursorPhone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
        }

        cursorPhone.close();

        Log.d(TAG, "Contact Phone Number: " + contactNumber);
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == ALL_PERMISSION) {
            List<String> requiredPerm = new ArrayList<>();
            for (int i = 0; i < permissions.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    requiredPerm.add(permissions[i]);
                }
            }

            String[] per = new String[requiredPerm.size()];
            userResponse(requiredPerm.toArray(per));
        }

    }



    private void retrieveContactName() {

        String contactName = null;

        // querying contact data store
        Cursor cursor = getContentResolver().query(uriContact, null, null, null, null);

        if (cursor.moveToFirst()) {

            // DISPLAY_NAME = The display name for the contact.
            // HAS_PHONE_NUMBER =   An indicator of whether this contact has at least one phone number.

            contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
        }

        cursor.close();

        Log.d(TAG, "Contact Name: " + contactName);



    }
    private void signOut() {

     //   mGoogleApiClient = new GoogleApiClient.Builder(SettingActivity.this);


        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // ...
                    }
                });

    /*    mGoogleApiClient = new GoogleApiClient.Builder(SettingActivity.this)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {
                        //SIGN OUT HERE
                        Auth.GoogleSignInApi.signOut(mGoogleApiClient.build()).setResultCallback(
                                new ResultCallback<Status>() {
                                    @Override
                                    public void onResult(Status status) {*//*ignored*//*
                                        Toast.makeText(context,"Hello",Toast.LENGTH_SHORT).show();
                                    }
                                });
                    }

                    @Override
                    public void onConnectionSuspended(int i) {

                    }
                });*/




    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {


    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
