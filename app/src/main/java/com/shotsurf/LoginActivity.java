package com.shotsurf;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.google.android.gms.wearable.Wearable;
import com.google.gson.Gson;
import com.shotsurf.common.AppConstants;
import com.shotsurf.views.BaseActivity;
import com.shotsurf.views.CommonUtils;
import com.shotsurf.views.FacebookData;
import com.shotsurf.views.FacebookResponse;
import com.shotsurf.views.ForgotPassword;
import com.shotsurf.views.Password;
import com.shotsurf.views.UserName;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by shankar on 4/6/2017.
 */

public class LoginActivity extends BaseActivity implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks
        , GoogleApiClient.OnConnectionFailedListener {
    ImageView fb;



    EditText userName, password;
    String username, pass;
    Button login;
    String forgetEmail;

    private TextView tvFacebookSignup, tvSignEmail;
    private Intent intent;
    private  ImageView tvSignUpGoogle;
    TextView usernameValidation,passwordValidation;


    private String id, TAG = LoginActivity.class.getSimpleName();
    private static GoogleApiClient mGoogleApiClient;
    private int RC_SIGN_IN = 100;
    private boolean isFbClicked = false, isGPlusClicked = false;

    private boolean mIntentInProgress;
    private String socialName = "", socialEmail = "", socialProfileUrl = "", socialGender = "", socialType = "", socialID = "", socialDOB = "", toShowDOB = "";
    private ArrayList<String> facebookIDs;
    private final int SPLASH_DISPLAY_LENGTH = 1500;
    private GoogleApiClient mGoogleApiClient1;

    /*Facebook*/


    private CallbackManager callbackManager;
    private AccessToken accessToken;
    private Context context;
    private Activity mcontext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this);
        setContentView(R.layout.activity_login);
        context=LoginActivity.this;
        mcontext=LoginActivity.this;
        printKeyHash(mcontext);
        getIds();

        setGooogleApi();
        setListeners();
        usernameValidation=(TextView)findViewById(R.id.user_name_validity);
        passwordValidation=(TextView)findViewById(R.id.password_validity);


    }

    private void getIds() {
        userName = (EditText) findViewById(R.id.userName);
        password = (EditText) findViewById(R.id.password);
        String styledText = "Don't have an account?<font color=\"#4D72AC\">Sign Up</font>";
        String text = "Forgot Password?<font color=\"#4D72AC\">Click here.</font>";
        ((TextView) findViewById(R.id.txtSignUp)).setText(Html.fromHtml(styledText));
        ((TextView) findViewById(R.id.txtForgot)).setText(Html.fromHtml(text));
        login=(Button)findViewById(R.id.btnLogin);
        tvSignUpGoogle = (ImageView) findViewById(R.id.tvGoogle);

        fb = (ImageView) findViewById(R.id.facebook);
        ((TextView) findViewById(R.id.txtSignUp)).setText(Html.fromHtml(styledText));
        ((TextView) findViewById(R.id.txtForgot)).setText(Html.fromHtml(text));

    }

    private void setGooogleApi() {

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestScopes(new Scope(Scopes.PROFILE))
                .requestScopes(new Scope(Scopes.PLUS_LOGIN))
                .requestProfile()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addApi(Plus.API)
                .build();
    }

    /*Forgot password dialog here*/
    private void openForgotPassDialog() {
        final Dialog dialog = new Dialog(LoginActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_forgot_pass);
        final EditText forget = (EditText) dialog.findViewById(R.id.edtEmail);
        Button button = (Button) dialog.findViewById(R.id.btnLogin);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forgetEmail = forget.getText().toString();
                if (forgetEmail != null) {
                    new ForgetTask().execute();

                }

            }
        });
        dialog.setCancelable(false);

        dialog.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    @Override
    protected void onStart() {
        super.onStart();

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (callbackManager != null && isFbClicked) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
            isFbClicked = false;
        }
        if (requestCode == RC_SIGN_IN) {
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                handleSignInResult(result);
                isGPlusClicked = false;


        }
//            if (requestCode == RC_SIGN_IN) {
//                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
//                handleSignInResult(result);
//                isGPlusClicked = false;
//
//
//        }

    }

    class LoginTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(" ");
        }

        @Override
        protected String doInBackground(String... params) {
            final OkHttpClient okHttpClient = new OkHttpClient();
            RequestBody formBody = new FormBody.Builder()
                    .add("password", pass)
                    .add("action", "login")
                    .add("email", username)
                    .add("device_token", "xyz")
                    .add("device_type", "android")
                    .build();
            Request request = new Request.Builder()
                    .url("http://www.404coders.com/shotsurf/services/services.php")
                    .post(formBody)
                    .build();
            try {
                Response response = okHttpClient.newCall(request).execute();
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                return response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }


        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            hideProgress();
            Log.e("Value", " " + s);
            if (s != null) {


                try {


                    JSONObject jsonObject = new JSONObject(s);
                    String response = jsonObject.getString("responseMessage");
                    toast(response);
                    if (response.equals("Successfully Login")) {
                        startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                        finish();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else
            {
                toast("Server not responding");
            }
        }



    }



    class ForgetTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            final OkHttpClient okHttpClient = new OkHttpClient();
            RequestBody formBody = new FormBody.Builder()
                    .add("email", forgetEmail)
                    .add("action", "forgotPassword")
                    .build();
            Request request = new Request.Builder()
                    .url("http://www.404coders.com/shotsurf/services/services.php")
                    .post(formBody)
                    .build();
            try {
                Response response = okHttpClient.newCall(request).execute();
                if (!response.isSuccessful())
                    throw new IOException("Unexpected code " + response);
                return response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress("");
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            hideProgress();
            Log.e("Value", " " + s);
            try {


                JSONObject jsonObject = new JSONObject(s);
                String Code = jsonObject.getString("Code");

                if (Code!=null) {
                    Intent intent = new Intent(LoginActivity.this, Password.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("Email",forgetEmail);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    finish();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void setListeners() {
        tvSignUpGoogle.setOnClickListener(this);
        fb.setOnClickListener(this);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usernameValidation.setVisibility(View.GONE);
                passwordValidation.setVisibility(View.GONE);
                username = userName.getText().toString();
                pass = password.getText().toString();

                if(!TextUtils.isEmpty(username)&&(!TextUtils.isEmpty(pass))){
                    new LoginTask().execute();
                }

                else if(TextUtils.isEmpty(username)) {
                    usernameValidation.setVisibility(View.VISIBLE);
                    usernameValidation.setText("Please enter your Username, Phone no, Email!");


                }
                else if(TextUtils.isEmpty(pass))
                {
                   passwordValidation.setVisibility(View.VISIBLE);
                    passwordValidation.setText("Please enter your Password!");
                }
            }
        });


        findViewById(R.id.txtSignUp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RegistrationActivity.class));
                finish();
            }
        });
        findViewById(R.id.txtForgot).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, ForgotPassword.class));
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.facebook:
                isFbClicked = true;
                loginWithFacebook();

                break;
            case R.id.tvGoogle:
                isGPlusClicked = true;
                Auth.GoogleSignInApi.signOut(mGoogleApiClient);
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
                break;
        }
    }

    private void loginWithFacebook() {
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logOut();
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email", "user_birthday", "user_friends"));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                accessToken = AccessToken.getCurrentAccessToken();
                GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {

                        Log.e(TAG, " **graphResponse** " + response);

                        String id = response.getJSONObject().optString("id");
                        String image_value = "https://graph.facebook.com/" + id + "/picture?type=large";

                        String first_name = response.getJSONObject().optString("first_name");
                        String last_name = response.getJSONObject().optString("last_name");
                        String user_name = response.getJSONObject().optString("name");
                        String genderFB = response.getJSONObject().optString("gender");
                        String email = response.getJSONObject().optString("email");
                        String birthday = response.getJSONObject().optString("birthday");

                        Log.e(TAG, " **FacebookAppId" + id);


                        try {
                            JSONObject friends = response.getJSONObject().getJSONObject("friends");
                            // JSONObject coverPic = response.getJSONObject().getJSONObject("cover");

                            Log.e(TAG, " **friendsResponse** " + new Gson().toJson(friends));
                            Log.e(TAG, " **FacebookAppId" + id);

                            FacebookResponse facebookResponse = new Gson().fromJson(friends.toString(), FacebookResponse.class);

                            facebookIDs = new ArrayList<>();

                            if (facebookResponse != null) {
                                facebookIDs.clear();
                                if (facebookResponse.data != null && facebookResponse.data.size() > 0) {
                                    for (FacebookData facebookData : facebookResponse.data) {
                                        facebookIDs.add(facebookData.id);
                                    }

                                }
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        Log.e(TAG, " **gender** " + genderFB + " **birthday** " + birthday);
                        String fbDOB = "";
                        if (!TextUtils.isEmpty(birthday)) {

                        }


                        if (!TextUtils.isEmpty(email)) {
                            email = response.getJSONObject().optString("email");
                            socialName = user_name;
                            socialEmail = email;
                            socialProfileUrl = image_value;
                            socialGender = genderFB;
                            socialType = "facebook";
                            socialID = id;
                            socialDOB = fbDOB;

                            showProgress("Please wait");

                            new facebook().execute();

                        }
                        else {
                            Toast.makeText(context, "Sorry! Your email is set private in facebook. Please make it public to use", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,first_name,last_name,name,gender,email,picture.width(200),birthday,friends{name}");
                request.setParameters(parameters);
                request.executeAsync();


            }

            @Override
            public void onCancel() {
                //Toast.makeText(getApplication(), "Cancelled", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException e) {
               //c Toast.makeText(getApplication(), "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }



    @Override
    public void onConnected(@Nullable Bundle bundle) {
        //getProfileInformation();
    }

    @Override
    public void onConnectionSuspended(int i) {
        //mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    /*    if (!mIntentInProgress && connectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                startIntentSenderForResult(connectionResult.getResolution().getIntentSender(),
                        RC_SIGN_IN, null, 0, 0, 0);
            } catch (IntentSender.SendIntentException e) {
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
        }*/
    }





    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();

            if (acct != null) {
                Person person = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
                socialName = acct.getDisplayName();
                socialEmail = acct.getEmail();
                String gender = String.valueOf(person.getGender());
                if (gender.equalsIgnoreCase("0")) {
                    socialGender = "male";
                } else {
                    socialGender = "female";
                }
                // socialGender = "male";
                socialProfileUrl = String.valueOf(acct.getPhotoUrl());
                socialType = "googlePlus";
                socialID = acct.getId();
                socialDOB = "";
                toShowDOB = "";

                Log.e(TAG, "socialEmail" + socialEmail + "socialProfileUrl" +
                        String.valueOf(acct.getPhotoUrl()) + "socialID" + socialID + " gender " + gender);




                showProgress("Please wait");


                new googleLogin().execute();




               /* if (CommonUtils.isOnline(context)) {
                  //  callSocialLoginApi(acct.getId(), acct.getEmail());
                } else {
                   // CommonUtils.showToast(context, getString(R.string.please_check_internet_connection));
                }*/
            }

        } else {

            // CommonUtils.showToast(context, "Unauthorized access");
        }
    }



    public  String printKeyHash(Activity context) {
        PackageInfo packageInfo;
        String key = null;
        try {
            //getting application package name, as defined in manifest
            String packageName = context.getApplicationContext().getPackageName();

            //Retriving package info
            packageInfo = context.getPackageManager().getPackageInfo(packageName,
                    PackageManager.GET_SIGNATURES);

            Log.e("Package Name=", context.getApplicationContext().getPackageName());

            for (Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));
                // String key = new String(Base64.encodeBytes(md.digest()));
                Log.e("Key Hash::::=", key);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("Name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("No such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }

        return key;
    }


   class googleLogin extends AsyncTask<String,Void,String>
   {

       @Override
       protected String doInBackground(String... params) {
           final OkHttpClient okHttpClient = new OkHttpClient();
           RequestBody formBody = new FormBody.Builder()
                   .add("action", "socialMedia")
                   .add("email",socialEmail )
                   .add("type", "google")
                   .add("facebookID",socialID)
                   .build();
           Request request = new Request.Builder()
                   .url("http://www.404coders.com/shotsurf/services/services.php")
                   .post(formBody)
                   .build();
           try {
               Response response = okHttpClient.newCall(request).execute();
               if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
               return response.body().string();
           } catch (IOException e) {
               e.printStackTrace();
               return null;
           }

       }

       @Override
       protected void onPreExecute() {
           super.onPreExecute();


       }

       @Override
       protected void onPostExecute(String servicesResponse) {
           super.onPostExecute(servicesResponse);

           hideProgress();

           if (servicesResponse != null) {
               try {
                   JSONObject jsonObject = new JSONObject(servicesResponse);

                   String userID = jsonObject.getString("userID");
                   String responseCode = jsonObject.getString("responseCode");
                   String message = jsonObject.getString("responseMessage");

                   Log.e("TAG", "facebook" + servicesResponse);


                   if (!TextUtils.isEmpty(responseCode)) {

                       if (responseCode.equalsIgnoreCase("200")) {

                           if (!TextUtils.isEmpty(userID)) {

                               toast(message);
                               CommonUtils.savePreferencesString(context, AppConstants.USER_ID, userID);
                               intent = new Intent(context, UserName.class);
                               intent.putExtra("socialName", socialName);
                               intent.putExtra("socialEmail", socialEmail);
                               intent.putExtra("socialProfileUrl", socialProfileUrl);
                               intent.putExtra("socialGender", socialGender);
                               intent.putExtra("socialType", socialType);
                               intent.putExtra("socialID", socialID);
                               intent.putExtra("socialDOB", socialDOB);
                               intent.putExtra("toShowDOB", toShowDOB);
                               if (facebookIDs != null) {
                                   intent.putStringArrayListExtra("facebookIDs", facebookIDs);
                               }
                               intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                               startActivity(intent);
                           } else {

                               intent = new Intent(context, HomeActivity.class);
                               intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                               startActivity(intent);
                           }
                       } else {

                           if (!TextUtils.isEmpty(userID)) {
                               intent = new Intent(context, HomeActivity.class);
                               intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                               startActivity(intent);
                           }
                       }
                   }
               } catch (JSONException e) {
                   e.printStackTrace();
               }

           }
           else
           {
             toast("Server not Responding.");
           }
       }

   }

    class facebook extends AsyncTask<String,Void,String>
    {

        @Override
        protected String doInBackground(String... params) {
            final OkHttpClient okHttpClient = new OkHttpClient();
            RequestBody formBody = new FormBody.Builder()
                    .add("action", "socialMedia")
                    .add("email",socialEmail )
                    .add("type", "facebook")
                    .add("facebookID",socialID)
                    .build();
            Request request = new Request.Builder()
                    .url("http://www.404coders.com/shotsurf/services/services.php")
                    .post(formBody)
                    .build();
            try {
                Response response = okHttpClient.newCall(request).execute();
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                return response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(String servicesResponse) {
            super.onPostExecute(servicesResponse);

         hideProgress();

            Log.e("TAG", "facebookSocial" + servicesResponse);

            if (!TextUtils.isEmpty(servicesResponse)) {


                try {
                    JSONObject jsonObject = new JSONObject(servicesResponse);

                    String userID = jsonObject.getString("userID");
                    String responseCode = jsonObject.getString("responseCode");
                    String message = jsonObject.getString("responseMessage");

                    Log.e("TAG", "facebook" + servicesResponse);


                    if (!TextUtils.isEmpty(responseCode)) {

                        if (responseCode.equalsIgnoreCase("200")) {

                            if (!TextUtils.isEmpty(userID)) {

                                toast(message);
                                CommonUtils.savePreferencesString(context, AppConstants.USER_ID, userID);
                                intent = new Intent(context, UserName.class);
                                intent.putExtra("socialName", socialName);
                                intent.putExtra("socialEmail", socialEmail);
                                intent.putExtra("socialProfileUrl", socialProfileUrl);
                                intent.putExtra("socialGender", socialGender);
                                intent.putExtra("socialType", socialType);
                                intent.putExtra("socialID", socialID);
                                intent.putExtra("socialDOB", socialDOB);
                                intent.putExtra("toShowDOB", toShowDOB);
                                if (facebookIDs != null) {
                                    intent.putStringArrayListExtra("facebookIDs", facebookIDs);
                                }
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            } else {
                                intent = new Intent(context, HomeActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            }


                        }
                        else {

                            if (!TextUtils.isEmpty(userID)) {

                                intent = new Intent(context, HomeActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            else
            {
                toast("Server not Responding.");
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        EditText editText = (EditText)findViewById(R.id.userName);
        editText.setText("");
        EditText neweditText = (EditText)findViewById(R.id.password);
        neweditText.setText("");
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }


    public  void getLogout() {

        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks( new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle connectionHint) {
                        Log.i( "", "onConnected: " + connectionHint);

                        Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
                        Plus.AccountApi.revokeAccessAndDisconnect(mGoogleApiClient);
                        mGoogleApiClient.disconnect();
                        // Now you can use the Data Layer API
                    }
                    @Override
                    public void onConnectionSuspended(int cause) {
                        Log.i( "", "onConnectionSuspended: " + cause);
                    }
                })
                .addOnConnectionFailedListener( new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult result) {
                        Log.i( "", "onConnectionFailed: " + result);
                    }
                })
                // Request access only to the Wearable API
                .addApi(Wearable.API)
                .build();







        /*  if (mGoogleApiClient.isConnected()) {
                Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
                Plus.AccountApi.revokeAccessAndDisconnect(mGoogleApiClient);
                mGoogleApiClient.disconnect();
            }*/

    }
    public static   void setLogout(){



    }
    @Override
    protected void onResume() {
        super.onResume();
       // Toast.makeText(context, "logoutGoogle", Toast.LENGTH_SHORT).show();

    }
}
