package com.shotsurf;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.shotsurf.common.AppConstants;
import com.shotsurf.views.BaseActivity;
import com.shotsurf.views.CommonUtils;
import com.shotsurf.views.Main3Activity;
import com.shotsurf.views.MainActivity;
import com.shotsurf.views.Password;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class RegistrationActivity extends BaseActivity implements TextWatcher {
Button nextButton,verifyOtp;
    int position = 1;
    Context context;
    EditText email,phone;
    TextView phoneValidation,emailValidation;
    String emailText,s;
    private String newvalue="",number="",emailId=" ";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=RegistrationActivity.this;
        setContentView(R.layout.activity_registration);
        nextButton=(Button)findViewById(R.id.phone_otp) ;
        verifyOtp=(Button)findViewById(R.id.email_otp) ;
        inflateXmlData();
        phone=(EditText)findViewById(R.id.email_id);
        emailValidation=(TextView)findViewById(R.id.email_validity);
        phoneValidation=(TextView)findViewById(R.id.phone_no_validity);
        CommonUtils.savePreferencesString(context, AppConstants.KEY_PHONE, "");
        CommonUtils.savePreferencesString(context, AppConstants.KEY_EMAIL, "");
//        toast("EMAIL"+CommonUtils.getPreferences(context,AppConstants.KEY_EMAIL));

        newvalue = email.getText().toString();
      number=newvalue;


    }




    private void inflateXmlData() {
        String styledText = "Already have an account? <font color=\"#4D72AC\">Sign In</font>";
        ((TextView) findViewById(R.id.txtSignUp)).setText(Html.fromHtml(styledText));
        ((TextView) findViewById(R.id.txtSignUp)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegistrationActivity.this, LoginActivity.class));
            }
        });


        email = ((EditText) findViewById(R.id.password));

        number = email.getText().toString();


//        findViewById(R.id.emailBtn).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                position++;
//                ((TextView) findViewById(R.id.txtPhone)).setTextColor(getResources().getColor(R.color.divider));
//                findViewById(R.id.linePhone).setBackgroundColor(getResources().getColor(R.color.divider));
//                ((TextView) findViewById(R.id.txtEmail)).setTextColor(getResources().getColor(R.color.black));
//                findViewById(R.id.lineEmail).setBackgroundColor(getResources().getColor(R.color.black));
//                ((EditText) findViewById(R.id.password)).setHint("Email address");
//                ((EditText) findViewById(R.id.password)).setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_SUBJECT);
//
//            }
//        });
//
//        findViewById(R.id.phoBox).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ((TextView) findViewById(R.id.txtPhone)).setTextColor(getResources().getColor(R.color.black));
//                findViewById(R.id.linePhone).setBackgroundColor(getResources().getColor(R.color.black));
//                ((TextView) findViewById(R.id.txtEmail)).setTextColor(getResources().getColor(R.color.divider));
//                findViewById(R.id.lineEmail).setBackgroundColor(getResources().getColor(R.color.divider));
//                ((EditText) findViewById(R.id.password)).setHint("Phone number");
//                ((EditText) findViewById(R.id.password)).setInputType(InputType.TYPE_CLASS_NUMBER);
//                ((EditText) findViewById(R.id.password)).setFilters(new InputFilter[] {new InputFilter.LengthFilter(12)});
//
//
//
//            }
//        });
        verifyOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emailValidation.setVisibility(View.GONE);
                phoneValidation.setVisibility(View.GONE);
                emailText = phone.getText().toString();
                if (TextUtils.isEmpty(emailText)) {
                    emailValidation.setVisibility(View.VISIBLE);
                    emailValidation.setText("Please enter your email id");
                } else if (!(Patterns.EMAIL_ADDRESS).matcher(emailText).matches()) {
                    emailValidation.setVisibility(View.VISIBLE);
                    emailValidation.setText("Please enter your valid email id");

                }

//                else if (!emailText.matches(("\\b[\\w.%-]+@[-.\\w]+\\.[A-Za-z]{2,4}\\b")))
//                {
//
//                }



              //  String user[]=

             //  String s=emailText.split(".com",);



//                else if (!emailText.endsWith("com"))
//                {
//                    emailValidation.setVisibility(View.VISIBLE);
//                    emailValidation.setText("Please enter your valid email id");
//                }
//                else if (!emailText.endsWith("in"))
//                {
//                    emailValidation.setVisibility(View.VISIBLE);
//                    emailValidation.setText("Please enter your valid email id");
//                }
                else {
                    emailId=emailText;
                    CommonUtils.savePreferencesString(context, AppConstants.KEY_EMAIL, emailId);

                    new EmailOtp().execute();
                }
            }
        });
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emailValidation.setVisibility(View.GONE);
                phoneValidation.setVisibility(View.GONE);
                s = ((EditText) findViewById(R.id.password)).getText().toString();
                if (TextUtils.isEmpty(s)) {
                    phoneValidation.setVisibility(View.VISIBLE);
                    phoneValidation.setText("Please enter your phone number");
                } else if (s.length() < 10 || s.length() > 12) {
                    phoneValidation.setVisibility(View.VISIBLE);
                    phoneValidation.setText("Please Enter 10 digit phone number");


                    //   doValidation()
                }
//                else if(!emailText.contains(".com"))
//                {
//                    emailValidation.setVisibility(View.VISIBLE);
//                    emailValidation.setText("Please enter your valid email id");
//                }
//                else
//                {
//
//
//
//                    if(numberExists(context,number)){
//
//                        Toast.makeText(RegistrationActivity.this, "Number is already exits", Toast.LENGTH_SHORT).show();
//
//
//                    }
                else {

                    number=s;
                    CommonUtils.savePreferencesString(context, AppConstants.KEY_PHONE, number);


                    new PhoneOtp().execute();



                }


/*
                    if(number.equalsIgnoreCase(email.getText().toString()))
                    {

                        Toast.makeText(RegistrationActivity.this, "Number is already exits", Toast.LENGTH_SHORT).show();
                    }
                    else {

                        if (newvalue != null) {
                            new PhoneOtp().execute();
                        }
                    }*/


            }

        });
    }



//    public Boolean numberExists(Context context, String phoneNumber) {
//        String contactName = null;
//        ContentResolver resolver = context.getContentResolver();
//        Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
//        Cursor cursor = resolver.query(uri, new String[]{ContactsContract.Contacts.DISPLAY_NAME}, null, null, null);
//        try {
//
//            if (cursor != null) {
//                if (cursor.moveToFirst()) {
//                    return true;
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            if (cursor != null && !cursor.isClosed()) {
//                cursor.close();
//            }
//        }
//        return false;
//    }


    private boolean doValidation() {
        if (email.getText().toString().trim().isEmpty()) {
            email.setText("Please enter email.");
            email.requestFocus();
            return false;
        }
        else if (Character.isLetter(email.getText().toString().trim().charAt(0))) {
            if (!CommonUtils.isValidEmail(email.getText().toString().trim())) {
                email.setText("Please enter a valid email.");
                email.requestFocus();
                return false;
            }
            else {


                return false;
            }
        }
        return true;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }
    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }
    @Override
    public void afterTextChanged(Editable s) {



    }
    class PhoneOtp extends AsyncTask<String, Void, String> {


        @Override
        protected String doInBackground(String... params) {
            final OkHttpClient okHttpClient = new OkHttpClient();
            RequestBody formBody = new FormBody.Builder()
                    .add("phone",s)
                    .add("action", "mobileEmailverfi")
                    .add("email","")
                    .build();
            Request request = new Request.Builder()
                    .url("http://www.404coders.com/shotsurf/services/services.php")
                    .post(formBody)
                    .build();
            try {
                Response response = okHttpClient.newCall(request).execute();
                if (!response.isSuccessful())
                    throw new IOException("Unexpected code " + response);
                return response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress("");
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            hideProgress();

            if(s!=null)
            try {


                JSONObject jsonObject = new JSONObject(s);
                Log.e("Value", " " + s);
                String Code = jsonObject.getString("Code");
                String msg = jsonObject.getString("responseMessage");
                String responseCode = jsonObject.getString("responseCode");
//                if(responseCode.equals("000"))
//                    toast("Already Exsit");


                Toast.makeText(context, msg, Toast.LENGTH_LONG).show();


                Log.e("Error", "otp " + msg);


                if (responseCode.equals("200")) {
                    Intent intent = new Intent(RegistrationActivity.this, Main3Activity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("OTP",Code);
                    if(!TextUtils.isEmpty(s))
                    intent.putExtras(bundle);
//                    Bundle bundle = new Bundle();
//                    bundle.putString("Email",forgetEmail);
//                    intent.putExtras(bundle);
                    startActivity(intent);
                }
//                else
//                {
//                    toast("User Already Exsit");
//                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    class EmailOtp extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            final OkHttpClient okHttpClient = new OkHttpClient();
            RequestBody formBody = new FormBody.Builder()
                    .add("email", emailText)
                    .add("action", "mobileEmailverfi")
                    .add("phone"," ")
                    .build();
            Request request = new Request.Builder()
                    .url("http://www.404coders.com/shotsurf/services/services.php")
                    .post(formBody)
                    .build();
            try {
                Response response = okHttpClient.newCall(request).execute();
                if (!response.isSuccessful())
                    throw new IOException("Unexpected code " + response);
                return response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress("");
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            hideProgress();
            Log.e("Value", " " + s);
            if(s!=null)
            try {


                JSONObject jsonObject = new JSONObject(s);
                String response = jsonObject.getString("responseMessage");
                String Code = jsonObject.getString("Code");
                String responseCode = jsonObject.getString("responseCode");
                Log.e("Error"," "+response);
                toast(response);
                if (responseCode.equals("200")) {

                    Intent intent = new Intent(RegistrationActivity.this, Main3Activity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("OTP",Code);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }

//                else
//                {
//                    toast("User Already Exsit");
//                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    @Override
    protected void onStop() {
        super.onStop();
        phone.setText("");
        EditText text =(EditText) findViewById(R.id.password);
        text.setText("");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(RegistrationActivity.this,LoginActivity.class));
    }
}
