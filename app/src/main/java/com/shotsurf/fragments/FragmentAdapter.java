package com.shotsurf.fragments;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


/**
 * Created by Admin on 9/28/2017.
 */

public class FragmentAdapter extends FragmentPagerAdapter {

    public FragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
       Fragment f= null;

        if(position==0)
        {
            f = new NewFragment();
        }
        else if(position==1)
        {
            f = new CameraFragment();
        }


        return f;
    }
    @Override
    public int getCount() {
        return 2;
    }
}
