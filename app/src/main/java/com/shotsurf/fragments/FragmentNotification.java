package com.shotsurf.fragments;



import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.shotsurf.R;
import com.shotsurf.bean.PostData;

import java.util.ArrayList;
import java.util.List;

public class FragmentNotification extends Fragment {
    private RecyclerView recyclerView;
    private View rootView;
    private List<PostData> postDataList;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_notification, null);

        postDataList = new ArrayList<>();
        postDataList = getPostDataList();
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        NGOAdapter ngoAdapter = new NGOAdapter();
        recyclerView.setAdapter(ngoAdapter);
        rootView.findViewById(R.id.emailBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((TextView) rootView.findViewById(R.id.txtPhone)).setTextColor(getResources().getColor(R.color.divider));
                rootView.findViewById(R.id.linePhone).setBackgroundColor(getResources().getColor(R.color.divider));
                ((TextView) rootView.findViewById(R.id.txtEmail)).setTextColor(getResources().getColor(R.color.black));
                rootView.findViewById(R.id.lineEmail).setBackgroundColor(getResources().getColor(R.color.black));

            }
        });

        rootView.findViewById(R.id.phoBox).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((TextView) rootView.findViewById(R.id.txtPhone)).setTextColor(getResources().getColor(R.color.black));
                rootView.findViewById(R.id.linePhone).setBackgroundColor(getResources().getColor(R.color.black));
                ((TextView) rootView.findViewById(R.id.txtEmail)).setTextColor(getResources().getColor(R.color.divider));
                rootView.findViewById(R.id.lineEmail).setBackgroundColor(getResources().getColor(R.color.divider));
            }
        });

        return rootView;
    }
    private List<PostData> getPostDataList() {
        List<PostData> postList = new ArrayList<>();
        PostData postData1 = new PostData();
        postData1.setUserName("kaira liked your comment: Nice look");
        postList.add(postData1);

        PostData postData2 = new PostData();
        postData2.setUserName("amit liked your comment: Nice look");
        postList.add(postData2);

        PostData postData3 = new PostData();
        postData3.setUserName("manoj commented: Out of focus image dcfhhf djdjd djdj");
        postList.add(postData3);


        return postList;
    }

    private class NGOAdapter extends RecyclerView.Adapter<NGOAdapter.PostViewHolder> {

        @Override
        public int getItemCount() {
            return postDataList.size();
        }

        @Override
        public void onBindViewHolder(final NGOAdapter.PostViewHolder vh, final int i) {
            PostData data = postDataList.get(i);
            vh.ngoName.setText(data.getUserName());
        }
        @Override
        public NGOAdapter.PostViewHolder onCreateViewHolder(ViewGroup viewGroup, int arg1) {
            View itemView = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.notification_item, viewGroup, false);
            return new NGOAdapter.PostViewHolder(itemView);
        }

        public class PostViewHolder extends RecyclerView.ViewHolder {
            protected TextView ngoName;

            public PostViewHolder(View v) {
                super(v);
                ngoName = (TextView) v.findViewById(R.id.userName);
            }
        }
    }

}
