package com.shotsurf.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.shotsurf.EditProfileActivity;
import com.shotsurf.R;
import com.shotsurf.SettingActivity;
import com.shotsurf.bean.PostData;

import java.util.ArrayList;
import java.util.List;


public class FragmentProfile extends Fragment {
    private RecyclerView recyclerView;
    private List<PostData> postDataList;
    private ArrayList<Integer> itemsimg;
    private View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        postDataList = new ArrayList<>();
        postDataList = getPostDataList();
        inflateXmlData();
        return rootView;
    }

    private List<PostData> getPostDataList() {
        List<PostData> postList = new ArrayList<>();
        PostData postData1 = new PostData();
        postData1.setUserName("Karan");
        postData1.setTag("#karan");
        postData1.setTime("6 April 2017");
        postData1.setCommentCount(1);
        postData1.setLikeCount(1);
        postList.add(postData1);

        PostData postData2 = new PostData();
        postData2.setUserName("Kaira");
        postData2.setTag("#thaglife");
        postData2.setTime("5 April 2017");
        postData2.setCommentCount(2);
        postData2.setLikeCount(2);
        postList.add(postData2);

        PostData postData3 = new PostData();
        postData3.setUserName("Amit");
        postData3.setTag("#masti");
        postData3.setTime("4 April 2017");
        postData3.setCommentCount(4);
        postData3.setLikeCount(2);
        postList.add(postData3);


        return postList;
    }

    private void inflateXmlData() {
        itemsimg = new ArrayList<Integer>();
        itemsimg.add(R.drawable.img1);
        itemsimg.add(R.drawable.img2);
        itemsimg.add(R.drawable.img3);
        itemsimg.add(R.drawable.img1);
        itemsimg.add(R.drawable.img2);
        itemsimg.add(R.drawable.img3);
        itemsimg.add(R.drawable.img1);
        itemsimg.add(R.drawable.img2);
        itemsimg.add(R.drawable.img3);

        rootView.findViewById(R.id.editProfile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), EditProfileActivity.class));
            }
        });
        rootView.findViewById(R.id.imgSetting).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SettingActivity.class));
            }
        });
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        GridLayoutManager recyclerViewLayoutManager = new GridLayoutManager(getActivity(), 3);
        recyclerView.setLayoutManager(recyclerViewLayoutManager);
        profileAdapter profileAdapter = new profileAdapter();
        recyclerView.setAdapter(profileAdapter);

    }

    private class profileAdapter extends RecyclerView.Adapter<profileAdapter.PostViewHolder> {

        @Override
        public int getItemCount() {
            return itemsimg.size();
        }

        @Override
        public void onBindViewHolder(final profileAdapter.PostViewHolder vh, final int i) {
            vh.postMedia.setImageResource(itemsimg.get(i));

        }

        @Override
        public profileAdapter.PostViewHolder onCreateViewHolder(ViewGroup viewGroup, int arg1) {
            View itemView = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.event_item, viewGroup, false);
            return new profileAdapter.PostViewHolder(itemView);
        }

        public class PostViewHolder extends RecyclerView.ViewHolder {
            protected ImageView postMedia;

            public PostViewHolder(View v) {
                super(v);
                postMedia = (ImageView) v.findViewById(R.id.postMedia);
            }
        }
    }
}
