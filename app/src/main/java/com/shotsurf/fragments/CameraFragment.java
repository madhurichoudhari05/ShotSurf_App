package com.shotsurf.fragments;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;

import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.shotsurf.HomeActivity;
import com.shotsurf.R;

/**
 * Created by Admin on 9/28/2017.
 */

public class CameraFragment extends Fragment {


    private View rootView;
    Button button;
    private static final int MY_CAMERA_REQUEST_CODE = 100;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_IMAGE = 1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.camera, container, false);
//        button=(Button)rootView.findViewById(R.id.camera);
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                openCamera();
//            }
//        });
//
////        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
////                != PackageManager.PERMISSION_DENIED) {
////
////            openCamera();
////
////        } else {
////            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, MY_CAMERA_REQUEST_CODE);
////        }
//        Toast.makeText(getActivity(),"Message",Toast.LENGTH_SHORT).show();


        return rootView;
    }

    private void openCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
    }

    @Override
    public void onStart() {
        super.onStart();

    }


    @Override
    public void setUserVisibleHint(boolean userVisibleHint) {
        super.setUserVisibleHint(userVisibleHint);
        if (userVisibleHint) {
//            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
//                    == PackageManager.PERMISSION_DENIED) {
//                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, MY_CAMERA_REQUEST_CODE);
//
//
//
//            } else {
//                openCamera();

//                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, MY_CAMERA_REQUEST_CODE);


            if(isRead())
            {
                openCamera();
            }
            else
            {
                requestPermission();
            }
        }
    }
//    @Override
//    public void (boolean userVisibleHint) {
//        super.setUserVisibleHint(userVisibleHint);
//        if(userVisibleHint)
//        {
//            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
//                    != PackageManager.PERMISSION_DENIED) {
//
//                openCamera();
//
//
//
//            } else {
//
//                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, MY_CAMERA_REQUEST_CODE);
////                openCamera();
//            }
//        }

    public boolean isRead() {
        int result = ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;
        else
            return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_IMAGE) {

             startActivity(new Intent(getActivity(),HomeActivity.class));

            //
        }
    }
    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                android.Manifest.permission.READ_EXTERNAL_STORAGE)) {

        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.CAMERA},
                    REQUEST_IMAGE_CAPTURE);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
           openCamera();
        }
    }
    }