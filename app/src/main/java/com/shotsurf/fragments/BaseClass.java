package com.shotsurf.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Toast;

/**
 * Created by Admin on 10/5/2017.
 */

public class BaseClass extends Fragment {
    private ProgressDialog mDialog;

    //progress dialog
    //toast
    //snackbar
    // internet connectivity
    public void showProgress(String msg) {
        if (mDialog==null)
            mDialog = new ProgressDialog(getActivity());
        // mDialog.setTitle(title);
        mDialog.setMessage(msg);
        mDialog.setIndeterminate(true);
        mDialog.show();
    }

    public void hideProgress() {
        if (mDialog != null && mDialog.isShowing())
            mDialog.dismiss();
    }

    @Override
    public void onStop() {
        super.onStop();
        hideProgress();
    }

    public void toast(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    public void snack(View v, String msg) {
        Snackbar.make(v, msg, Snackbar.LENGTH_LONG).show();
    }

    public boolean isInternetAvailable() {
        ConnectivityManager cm = (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }
}
