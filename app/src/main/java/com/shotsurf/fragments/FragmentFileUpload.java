package com.shotsurf.fragments;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.app.Fragment;
import android.view.View;
import android.view.ViewGroup;

import com.shotsurf.R;

import java.io.File;

public class FragmentFileUpload extends Fragment {
    private View rootView;
    private String fileName = "shotSurf.jpg";
    private static final int CAMERA_PIC_REQUEST = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_file_upload, container, false);


       /* rootView.findViewById(R.id.likeBtn).setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            rootView.findViewById(R.id.layoutLike).setVisibility(View.VISIBLE);
            rootView.findViewById(R.id.linePhoBox).setVisibility(View.GONE);
            rootView.findViewById(R.id.lineCamera).setVisibility(View.GONE);
            rootView.findViewById(R.id.lineLike).setVisibility(View.VISIBLE);
        }
    });

        rootView.findViewById(R.id.phoBox).setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            rootView.findViewById(R.id.layoutPhoBox).setVisibility(View.VISIBLE);
            rootView.findViewById(R.id.layoutLike).setVisibility(View.GONE);

            rootView.findViewById(R.id.linePhoBox).setVisibility(View.VISIBLE);
            rootView.findViewById(R.id.lineCamera).setVisibility(View.GONE);
            rootView.findViewById(R.id.lineLike).setVisibility(View.GONE);


        }
    });*/

      /*  rootView.findViewById(R.id.layoutCamera).setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            selectImage();


        }
    });*/
        return rootView;

}

    private void selectImage() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    if (checkCameraHardware(getActivity())) {
                        int permissionStatus = ContextCompat.checkSelfPermission(getActivity(),
                                Manifest.permission.CAMERA);
                        if (permissionStatus != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(getActivity(),
                                    new String[]{Manifest.permission.CAMERA}, 1);

                        } else {
                            permissionStatus = ContextCompat.checkSelfPermission(getActivity(),
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE);
                            if (permissionStatus != PackageManager.PERMISSION_GRANTED) {
                                ActivityCompat.requestPermissions(getActivity(),
                                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
                            } else {
                                openCamera();
                            }
                        }
                    }
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void openCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        File f = new File(android.os.Environment.getExternalStorageDirectory(), fileName);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
        startActivityForResult(intent, CAMERA_PIC_REQUEST);
    }

    /**
     * Check if this device has a camera
     */
    private boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

}
