package com.shotsurf.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.shotsurf.CommentActivity;
import com.shotsurf.LiveEventActivity;
import com.shotsurf.NGOActivity;
import com.shotsurf.NGOProfile;
import com.shotsurf.PhoBoxActivity;
import com.shotsurf.R;
import com.shotsurf.UserDetailActivity;
import com.shotsurf.bean.PostData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 10/4/2017.
 */

public class NewFragment extends Fragment {
    private RecyclerView recyclerView;
    private List<PostData> postDataList;
    private ArrayList<Integer> itemsimg;
    private View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.new_fragment, null);
       // Toast.makeText(getContext(), "second", Toast.LENGTH_SHORT).show();
        postDataList = new ArrayList<>();
        postDataList = getPostDataList();
        inflateXmlData();
        return rootView;
    }

    private List<PostData> getPostDataList() {
        List<PostData> postList = new ArrayList<>();
        PostData postData1 = new PostData();
        postData1.setUserName("Karan");
        postData1.setTag("#karan");
        postData1.setTime("6 April 2017");
        postData1.setCommentCount(1);
        postData1.setLikeCount(1);
        postList.add(postData1);

        PostData postData2 = new PostData();
        postData2.setUserName("Kaira");
        postData2.setTag("#thaglife");
        postData2.setTime("5 April 2017");
        postData2.setCommentCount(2);
        postData2.setLikeCount(2);
        postList.add(postData2);

        PostData postData3 = new PostData();
        postData3.setUserName("Amit");
        postData3.setTag("#masti");
        postData3.setTime("4 April 2017");
        postData3.setCommentCount(4);
        postData3.setLikeCount(2);
        postList.add(postData3);


        return postList;
    }

    private void inflateXmlData() {
        itemsimg = new ArrayList<Integer>();
        itemsimg.add(R.drawable.img1);
        itemsimg.add(R.drawable.img2);
        itemsimg.add(R.drawable.img3);
//        rootView.findViewById(R.id.image).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(getActivity(), NGOActivity.class));
//            }
//        });
//
//        rootView.findViewById(R.id.title).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(getActivity(), NGOProfile.class));
//            }
//        });

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        NewFragment.PostsAdapter postsAdapter = new NewFragment.PostsAdapter();
        recyclerView.setAdapter(postsAdapter);
    }

     class PostsAdapter extends RecyclerView.Adapter<NewFragment.PostsAdapter.PostViewHolder> {

        @Override
        public int getItemCount() {
            return postDataList.size();
        }

        @Override
        public void onBindViewHolder(final NewFragment.PostsAdapter.PostViewHolder vh, final int i) {
            final PostData data = postDataList.get(i);
            vh.userName.setText(data.getUserName());
            vh.createdDate.setText(data.getTime());
            vh.likeCount.setText("Like");
            vh.commentCount.setText("Comment");
            vh.postMedia.setImageResource(itemsimg.get(i));

            final GestureDetector gd = new GestureDetector(getActivity(), new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onDown(MotionEvent e) {
                    return true;
                }

                @Override
                public boolean onDoubleTap(MotionEvent e) {

                    Animation pulse_fade = AnimationUtils.loadAnimation(getActivity(), R.anim.pulse_fade_in);
                    pulse_fade.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                            vh.heartAnim.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            vh.heartAnim.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    vh.heartAnim.startAnimation(pulse_fade);
                    vh.buttonLike.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.wink_like));
//                    likeCount.setText("3 Likes");
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    super.onLongPress(e);

                }

            });
            vh.postMedia.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return gd.onTouchEvent(event);
                }
            });
            vh.userImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getActivity(),UserDetailActivity.class));
                }
            });
            vh.layoutComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getActivity(),CommentActivity.class));
                }
            });
            vh.menuItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getActivity(),LiveEventActivity.class));
                }
            });
            vh.layoutLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getActivity(),PhoBoxActivity.class));
                }
            });
        }

        @Override
        public NewFragment.PostsAdapter.PostViewHolder onCreateViewHolder(ViewGroup viewGroup, int arg1) {
            View itemView = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.home_item, viewGroup, false);
            return new NewFragment.PostsAdapter.PostViewHolder(itemView);
        }
        public class PostViewHolder extends RecyclerView.ViewHolder {
            protected TextView userName, createdDate, postText, commentCount, likeCount, likePost;
            protected ImageView userImage, buttonLike, buttonComment, postMedia, heartAnim,menuItem;
            protected LinearLayout layoutComment,layoutLike;

            public PostViewHolder(View v) {
                super(v);
                userName = (TextView) v.findViewById(R.id.userName);
                userImage = (ImageView) v.findViewById(R.id.userProfilePic);
                createdDate = (TextView) v.findViewById(R.id.postTime);
                buttonLike = (ImageView) v.findViewById(R.id.btnLike);
                buttonComment = (ImageView) v.findViewById(R.id.btnComment);
                postText = (TextView) v.findViewById(R.id.rvPostText);
                postMedia = (ImageView) v.findViewById(R.id.postMedia);
                commentCount = (TextView) v.findViewById(R.id.textComment);
                likeCount = (TextView) v.findViewById(R.id.textLike);
                likePost = (TextView) v.findViewById(R.id.textLike);
                heartAnim = (ImageView) v.findViewById(R.id.heart_anim);
                layoutComment = (LinearLayout) v.findViewById(R.id.layoutComment);
                menuItem = (ImageView) v.findViewById(R.id.menuItem);
                layoutLike = (LinearLayout) v.findViewById(R.id.layoutLike);

            }
        }
    }
}
