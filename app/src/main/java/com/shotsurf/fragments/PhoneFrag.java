package com.shotsurf.fragments;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.shotsurf.LoginActivity;
import com.shotsurf.R;
import com.shotsurf.views.Main2Activity;
import com.shotsurf.views.Main3Activity;
import com.shotsurf.views.Main4Activity;
import com.shotsurf.views.Password;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Admin on 10/5/2017.
 */

public class PhoneFrag extends BaseClass {
    private View rootView;
    Button button;
    public static  EditText phoneno;
    String phoneNumber;
    TextView text;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.phone_fragment, container, false);

        button=(Button)rootView.findViewById(R.id.phone_otp);
        phoneno=(EditText)rootView.findViewById(R.id.phone_forget) ;
        text=(TextView)rootView.findViewById(R.id.forgot_phone_no_validity);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text.setVisibility(View.GONE);
                phoneNumber=(phoneno).getText().toString();
                if(TextUtils.isEmpty(phoneNumber))
                {
                    text.setVisibility(View.VISIBLE);
                    text.setText("Please enter phone number");

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            text.setVisibility(View.GONE);


                        }
                    },2000);



//                            text.setVisibility(View.VISIBLE);
//                            text.setText("Please enter phone number");
                        }

//                    text.setVisibility(View.VISIBLE);


                else if (phoneNumber.length() < 10 || phoneNumber.length() > 12) {
                    text.setVisibility(View.VISIBLE);
                    text.setText("Please Enter 10 digit phone number");
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            text.setVisibility(View.GONE);


                        }
                    },2000);

                }
                else
                {
                    new ForgetTask().execute();
                }
            }
        });
        return rootView;
    }
    class ForgetTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            final OkHttpClient okHttpClient = new OkHttpClient();
            RequestBody formBody = new FormBody.Builder()
                    .add("email", phoneNumber)
                    .add("action", "forgotPassword")
                    .build();
            Request request = new Request.Builder()
                    .url("http://www.404coders.com/shotsurf/services/services.php")
                    .post(formBody)
                    .build();
            try {
                Response response = okHttpClient.newCall(request).execute();
                if (!response.isSuccessful())
                    throw new IOException("Unexpected code " + response);
                return response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(" ");

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            hideProgress();
            if (s != null)
                Log.e("Value", " " + s);
            if (!TextUtils.isEmpty(s)) {
                try {


                    JSONObject jsonObject = new JSONObject(s);
                    String Code = jsonObject.getString("Code");
                    String response = jsonObject.getString("responseMessage");
                    toast(response);

                    if (response.equals("Otp sent on your phone.Please verify")) {
                        Intent intent = new Intent(getActivity(), Main4Activity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("Email", phoneNumber);
                        bundle.putString("OTP", Code);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                    else
                    {
//                        toast("Phone number does no exsit.");
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else
            {
                toast("Server not responding.");
            }
        }

    }

    @Override
    public void onResume() {
        super.onResume();


    }

}
