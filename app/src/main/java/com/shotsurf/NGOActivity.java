package com.shotsurf;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.shotsurf.bean.PostData;

import java.util.ArrayList;
import java.util.List;

public class NGOActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private ArrayList<Integer> itemsimg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ngo);
        ((TextView) findViewById(R.id.title)).setText("Non-Governmental Organization(NGO)");
        inflateXmlData();
    }

    private void inflateXmlData() {
        itemsimg = new ArrayList<>();
        itemsimg.add(R.drawable.img1);
        itemsimg.add(R.drawable.img2);
        itemsimg.add(R.drawable.img3);
        itemsimg.add(R.drawable.img1);
        itemsimg.add(R.drawable.img2);
        itemsimg.add(R.drawable.img3);
        itemsimg.add(R.drawable.img1);
        itemsimg.add(R.drawable.img2);
        itemsimg.add(R.drawable.img3);

        recyclerView = (RecyclerView) findViewById(R.id.ngoRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        NGOAdapter ngoAdapter = new NGOAdapter();
        recyclerView.setAdapter(ngoAdapter);

    }

    private class NGOAdapter extends RecyclerView.Adapter<NGOAdapter.PostViewHolder> {

        @Override
        public int getItemCount() {
            return itemsimg.size();
        }

        @Override
        public void onBindViewHolder(final NGOAdapter.PostViewHolder vh, final int i) {
            vh.ngoName.setText("Aadhaar – New Delhi");
            vh.ngoImage.setImageResource(itemsimg.get(i));

        }

        @Override
        public NGOAdapter.PostViewHolder onCreateViewHolder(ViewGroup viewGroup, int arg1) {
            View itemView = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.ngo_item, viewGroup, false);
            return new NGOAdapter.PostViewHolder(itemView);
        }

        public class PostViewHolder extends RecyclerView.ViewHolder {
            protected TextView ngoName;
            protected ImageView ngoImage;

            public PostViewHolder(View v) {
                super(v);
                ngoName = (TextView) v.findViewById(R.id.title);
                ngoImage = (ImageView) v.findViewById(R.id.image);
            }
        }
    }
/*
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }*/
}
