package com.shotsurf.views;

import android.provider.CalendarContract;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;

import com.shotsurf.R;
import com.shotsurf.fragments.EmailFrag;
import com.shotsurf.fragments.PhoneFrag;

public class ForgotPassword extends AppCompatActivity {
    ViewPager viewPager;
    FrogotAdapter frogotAdapter;
    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        viewPager =(ViewPager)findViewById(R.id.viewPager);
        tabLayout=(TabLayout)findViewById(R.id.tablayout);
        frogotAdapter=new FrogotAdapter(getSupportFragmentManager());
        viewPager.setAdapter(frogotAdapter);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {
            }
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            public void onPageSelected(int position) {
                if (position == 1) {
                    EmailFrag.emailId.setText("");

                }

                else {
                    PhoneFrag.phoneno.setText("");
                }
            }

            // Check if this is the page you want.

        });
    }
}
