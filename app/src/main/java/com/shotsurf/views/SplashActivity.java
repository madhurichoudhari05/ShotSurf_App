package com.shotsurf.views;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.shotsurf.HomeActivity;
import com.shotsurf.LoginActivity;
import com.shotsurf.R;
import com.shotsurf.common.AppConstants;


import java.io.IOException;


public class SplashActivity extends Activity {

    private Context context = SplashActivity.this;
    private final int SPLASH_DISPLAY_LENGTH = 1500;
    ;
    private GoogleCloudMessaging gcm;
    private String regId, TAG = SplashActivity.class.getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (!CommonUtils.getPreferencesBoolean(context, AppConstants.FIRST_TIME_TUTORIAL)) {
                    Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(mainIntent);
                    finish();
                 //   CommonUtils.savePreferencesBoolean(context, AppConstants.FIRST_TIME_TUTORIAL, true);

                } else {
                    Intent mainIntent = new Intent(SplashActivity.this, HomeActivity.class);
                    startActivity(mainIntent);
                    finish();
                }

                //  checkWhtherLoggedInOrNot();

            }
        }, SPLASH_DISPLAY_LENGTH);

    }

}
