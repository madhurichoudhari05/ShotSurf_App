package com.shotsurf.views;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.shotsurf.LoginActivity;
import com.shotsurf.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Password extends BaseActivity {
    String email, newPassword, confirmPassword;
    EditText newPass, confirmPass;
    Button changePass;
    TextView passwordValidation,confirmValidation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);
        Bundle bundle = getIntent().getExtras();
        passwordValidation=(TextView)findViewById(R.id.pass);
        confirmValidation=(TextView)findViewById(R.id.password);
        email = bundle.getString("Email");
        newPass = (EditText) findViewById(R.id.new_pass);
        confirmPass = (EditText) findViewById(R.id.cnfr_pass);
        changePass = (Button) findViewById(R.id.change_pass);
        changePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                passwordValidation.setVisibility(View.GONE);
                confirmValidation.setVisibility(View.GONE);
                newPassword = newPass.getText().toString();
                confirmPassword = confirmPass.getText().toString();
                 if(TextUtils.isEmpty(newPassword))
                {
                    passwordValidation.setVisibility(View.VISIBLE);
                    passwordValidation.setText("Please set your Password");
                }
//                else if(!pass.matches("[a-zA-Z0-9\\-#\\.\\(\\)\\/%&\\s]{6,12}") && !pass.matches("(?=.*[A-Z])(?=.*[0-9])[a-zA-Z\\d]+"))
//                {
//                    password.setError("Must contain atleast one uppercase and one digit");
//
//                }
                else if(!newPassword.matches("(?=.*[0-9])" + "(?=.*[A-Z])"
                        + "[a-zA-Z0-9]*"))

                {

                    if(!newPassword.contains("(?=.*[0-9])"))
                    {
                        passwordValidation.setVisibility(View.VISIBLE);
                        passwordValidation.setText("Atleast One Numer,One Uppercase and Characters.");
                    }
                    else if(!newPassword.contains("(?=.*?[A-Z])"))
                    {
                        passwordValidation.setVisibility(View.VISIBLE);
                        passwordValidation.setText("Atleast One UpperCase");
                    }

                }

                else if(newPassword.length()<6)
                {
                    passwordValidation.setVisibility(View.VISIBLE);
                    passwordValidation.setText("Atleast Six characters");
                }
                else if(TextUtils.isEmpty(confirmPassword))
                {
                    confirmValidation.setVisibility(View.VISIBLE);
                    confirmValidation.setText("Please confirm your password");
                }
                else if(!newPassword.equals(confirmPassword))
                {
                    confirmValidation.setVisibility(View.VISIBLE);
                    confirmValidation.setText("Passwords are not same");
                }
                else
                 {
                     new ConfirmPass().execute();
                 }
            }
        });


    }


    class ConfirmPass extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            final OkHttpClient okHttpClient = new OkHttpClient();
            RequestBody formBody = new FormBody.Builder()
                    .add("email", email)
                    .add("action", "passwordChange")
                    .add("password", confirmPassword )
                    .build();
            Request request = new Request.Builder()
                    .url("http://www.404coders.com/shotsurf/services/services.php")
                    .post(formBody)
                    .build();
            try {
                Response response = okHttpClient.newCall(request).execute();
                if (!response.isSuccessful())
                    throw new IOException("Unexpected code " + response);
                return response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress("");
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            hideProgress();
            Log.e("Value", " " + s);
            try {


                JSONObject jsonObject = new JSONObject(s);
                String Code = jsonObject.getString("responseMessage");

                if (Code.equals("Password changed successfully")) {
                    toast(Code);
                    startActivity(new Intent(Password.this,LoginActivity.class));

                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
