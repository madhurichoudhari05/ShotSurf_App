package com.shotsurf.views;

/**
 * Created by Admin on 9/26/2017.
 */

public interface  ApiListener {
    public void onResponse(String response, int responseCode);
}
