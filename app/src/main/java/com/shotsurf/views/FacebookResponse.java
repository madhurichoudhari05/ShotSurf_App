package com.shotsurf.views;

import java.io.Serializable;
import java.util.List;

/**
 * Created by shashank.kapsime on 1/4/16.
 */
public class FacebookResponse implements Serializable {

    public List<FacebookData> data;
    public String source;
}
