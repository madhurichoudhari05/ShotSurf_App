package com.shotsurf.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.shotsurf.LoginActivity;
import com.shotsurf.R;

import java.util.regex.Pattern;


public class MainActivity extends BaseActivity {

String phonenumber;
    EditText fullname,password,confirmPassword;
    Button nextButton;
    TextView nameValid,passwordValidation,confirmValidation;
    Pattern regex;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Bundle bundle =getIntent().getExtras();
//        phonenumber= bundle.getString("PhoneNumber");
//        Log.e("no"," "+phonenumber);
        regex= Pattern.compile("(?=.*?[0-9])");
        fullname=(EditText)findViewById(R.id.name);
        password=(EditText)findViewById(R.id.password);
        confirmPassword=(EditText)findViewById(R.id.confirm_password);
        confirmValidation=(TextView)findViewById(R.id.confirm_valid);
        nameValid=(TextView)findViewById(R.id.full_name);
        passwordValidation=(TextView)findViewById(R.id.valid_pass);
        nextButton=(Button)findViewById(R.id.next);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nameValid.setVisibility(View.GONE);
                passwordValidation.setVisibility(View.GONE);
                confirmValidation.setVisibility(View.GONE);
                String name =fullname.getText().toString();
                String pass = password.getText().toString();
                String confirmPass = confirmPassword.getText().toString();

                if(TextUtils.isEmpty(name))
                {
                  nameValid.setVisibility(View.VISIBLE);
                    nameValid.setText("Please Enter Your Name");
                }



            else if (!name.matches("[a-zA-Z ]+")) {
                    nameValid.setVisibility(View.VISIBLE);
                    nameValid.setText("Please Enter Alphabets Only");

                }
                else if(TextUtils.isEmpty(pass))
                {
                   passwordValidation.setVisibility(View.VISIBLE);
                    passwordValidation.setText("Please Enter Your Password");
                }
//                else if(!pass.matches("[a-zA-Z0-9\\-#\\.\\(\\)\\/%&\\s]{6,12}") && !pass.matches("(?=.*[A-Z])(?=.*[0-9])[a-zA-Z\\d]+"))
//                {
//                    password.setError("Must contain atleast one uppercase and one digit");
//
//                }
                else if(!pass.matches("(?=.*[0-9])" + "(?=.*[A-Z])"
                                                        + "[a-zA-Z0-9]*"))

                {

                  if(!regex.matcher(pass).find())
                  {
                      passwordValidation.setVisibility(View.VISIBLE);
                      passwordValidation.setText("Atleast One Number");
                  }
                  else if(!pass.matches("(?=.*?[A-Z])"))
                    {
                        passwordValidation.setVisibility(View.VISIBLE);
                        passwordValidation.setText("Atleast One UpperCase");
                    }

                }

                else if(pass.length()<6)
                {
                    passwordValidation.setVisibility(View.VISIBLE);
                    passwordValidation.setText("Atleast Six Characters");
            }
            else if(TextUtils.isEmpty(confirmPass))
                {
                    confirmValidation.setVisibility(View.VISIBLE);
                    confirmValidation.setText("Please Confirm Your Password");
                }
                else if(!pass.equals(confirmPass))
                {
                    confirmValidation.setVisibility(View.VISIBLE);
                    confirmValidation.setText("Password Not Matched");
                }
                else
                {
                    Intent intent = new Intent(MainActivity.this, Main2Activity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("Name",name);
                    bundle.putString("Pass",pass);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            }
        });

    }

    @Override
    protected void onStop() {
        super.onStop();
        fullname.setText("");
        password.setText("");
        confirmPassword.setText("");

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(MainActivity.this, LoginActivity.class));
    }
}
