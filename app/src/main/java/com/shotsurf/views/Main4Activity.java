package com.shotsurf.views;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.shotsurf.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.R.attr.value;

public class Main4Activity extends BaseActivity {
    String value;
    EditText otpText;
    Button confirm,resend;
    String newvalue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);
        Bundle bundle = getIntent().getExtras();
        value = bundle.getString("OTP");
        newvalue=bundle.getString("Email");
        Log.e("Value"," "+value);
        otpText = (EditText) findViewById(R.id.confirmation);
        otpText.setText(value);

        confirm = (Button) findViewById(R.id.next_confirm);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Main4Activity.this, Password.class);
                Bundle bundle = new Bundle();
                bundle.putString("Email",newvalue);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        resend=(Button)findViewById(R.id.resend_otp);
        resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ForgetTask().execute();
            }
        });



    }
    class ForgetTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            final OkHttpClient okHttpClient = new OkHttpClient();
            RequestBody formBody = new FormBody.Builder()
                    .add("email", newvalue)
                    .add("action", "forgotPassword")
                    .build();
            Request request = new Request.Builder()
                    .url("http://www.404coders.com/shotsurf/services/services.php")
                    .post(formBody)
                    .build();
            try {
                Response response = okHttpClient.newCall(request).execute();
                if (!response.isSuccessful())
                    throw new IOException("Unexpected code " + response);
                return response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(" ");

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            hideProgress();
            if (s != null)
                Log.e("Value", " " + s);
            if (!TextUtils.isEmpty(s)) {
                try {


                    JSONObject jsonObject = new JSONObject(s);
                    String Code = jsonObject.getString("Code");
                    String response = jsonObject.getString("responseMessage");
                    toast(response);

                    if (response.equals("Otp sent on your mail.Please verify")) {

                       otpText.setText(Code);
                    }
                    else if(response.equals("Otp sent on your phone.Please verify"))
                    {
                        otpText.setText(Code);
                    }
                    {
//                        toast("Phone number does no exsit.");
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else
            {
                toast("Server not responding.");
            }
        }

    }


}
