//package com.shotsurf.views;
//
//import android.app.Activity;
//import android.app.ProgressDialog;
//import android.util.Log;
//import android.widget.Toast;
//
//import com.android.volley.DefaultRetryPolicy;
//import com.android.volley.Request;
//import com.android.volley.Response;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.StringRequest;
//import com.android.volley.toolbox.Volley;
//
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.Random;
//
///**
// * Created by Admin on 9/26/2017.
// */
//
//public class ApiHelper {
//    Activity mActivity;
//    ApiListener listner;
//    ProgressDialog dialog;
//
//    public ApiHelper(Activity activity, ApiListener listner) {
//        this.mActivity = activity;
//        this.listner = listner;
//        dialog = new ProgressDialog(mActivity);
//    }
//
//    public void requestPostStringApi(String url, final Map<String, String> params, final int apiCode, boolean isDialog) {
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        closeDialog();
//                        Log.e("BEFORE_SAVE_RESPONSE", response.toString());
//                        listner.onResponse(response, apiCode);
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        closeDialog();
//                        Toast.makeText(mActivity, "Server not responding", Toast.LENGTH_SHORT).show();
//                    }
//                }) {
//           /* @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String, String> params = new HashMap<String, String>();
//                //params.put("Content-Type", "application/json; charset=utf-8");
//                params.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
//                return params;
//            }*/
//
//            @Override
//            public String getBodyContentType() {
//                return "application/x-www-form-urlencoded";
//            }
//
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                return params;
//            }
//        };
//        /*try {
//            RequestQueue queue = Volley.newRequestQueue(mActivity);
//            int socketTimeout = 15000;//15 seconds - change to what you want
//            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//            stringRequest.setRetryPolicy(policy);
//            stringRequest.setShouldCache(false);
//            queue.add(stringRequest);
//            if (isDialog) {
//                showDialog("Please wait...");
//            }
//        } catch (OutOfMemoryError | Exception error) {
//            error.printStackTrace();
//        }*/
//        VolleySingleton.getInstance(mActivity).addToRequestQueue(stringRequest);
//        stringRequest.setRetryPolicy(new DefaultRetryPolicy(3* DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
//        if (isDialog) {
//            showDialog("Please wait...");
//        }
//
//
//    }
//
//    public void requestGetStringApi(String url, final int apiCode, boolean isDialog) {
//        StringRequest stringRequest = new StringRequest(url,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        closeDialog();
//                        Log.e("BEFORE_SAVE_RESPONSE", response.toString());
//                        listner.onResponse(response, apiCode);
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        closeDialog();
//                        //Toast.makeText(mActivity, "Server not responding", Toast.LENGTH_SHORT).show();
//                    }
//                });
//        RequestQueue queue = Volley.newRequestQueue(mActivity);
//        stringRequest.setShouldCache(false);
//        queue.add(stringRequest);
//        if (isDialog) {
//            showDialog("Please wait...");
//        }
//    }
//
//    public void multipartCalling(String url, final Map<String, String> params, final String filepath, final boolean isFileImg, boolean isDialog) {
//        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url,
//                new Response.Listener<NetworkResponse>() {
//                    @Override
//                    public void onResponse(NetworkResponse response) {
//                        closeDialog();
//                        String resultResponse = new String(response.data);
//                        listner.onResponse(resultResponse, AppConstants.MULTI_RESPONSE_CODE);
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        closeDialog();
//                        error.printStackTrace();
//                    }
//                }) {
//            @Override
//            protected Map<String, String> getParams() {
//                return params;
//            }
//
//            @Override
//            protected Map<String, DataPart> getByteData() {
//                Map<String, DataPart> params = new HashMap<>();
//                // file name could found file base or direct access from real path
//                // for now just get bitmap data from ImageView
//
//                if (isFileImg) {
//                    Random ran = new Random();
//                    int x = ran.nextInt(6) + 5;
//
//                    params.put("uploadedfile", new DataPart("uploadedfile.jpg", GlobalAccess.getByteDataFromFile(filepath), "image/jpeg"));
//
//                } else {
//                    //params.put("profile_pic", new DataPart("profile_pic.jpg", GlobalAccess.getByteDataFromUrl(filepath), "image/jpeg"));
//                    return null;
//
//                }
//
//
//                return params;
//            }
//        };
//
//        multipartRequest.setRetryPolicy(new DefaultRetryPolicy(
//                10000,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//
//        VolleySingleton.getInstance(mActivity.getBaseContext()).addToRequestQueue(multipartRequest);
//        if (isDialog) {
//            showDialog("Please wait...");
//        }
//    }
//
////    public void multipartMultiCalling(String url, final Map<String, String> params, final Map<String, List<String>> filepaths, boolean isDialog) {
////        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url,
////                new Response.Listener<NetworkResponse>() {
////                    @Override
////                    public void onResponse(NetworkResponse response) {
////                        closeDialog();
////                        String resultResponse = new String(response.data);
////                        listner.onResponse(resultResponse, AppConstants.MULTI_RESPONSE_CODE);
////                    }
////                },
////                new Response.ErrorListener() {
////                    @Override
////                    public void onErrorResponse(VolleyError error) {
////                        closeDialog();
////                        error.printStackTrace();
////                    }
////                }) {
////            @Override
////            protected Map<String, String> getParams() {
////                return params;
////            }
////
////            @Override
////            protected Map<String, List<DataPart>> getByteListData() throws AuthFailureError {
////              /* Map<String, List<DataPart>> dataParts = new HashMap<>();
////                for(Map.Entry<String, List<String >> entry: filepaths.entrySet()){
////                    List<DataPart> datas = new ArrayList<>();
////                    List<String> fileList = entry.getValue();
////                    for(int i=0;i<fileList.size();i++){
////                        datas.add(new DataPart("img_"+(i+1)+".jpg", GlobalAccess.getByteDataFromFile(fileList.get(i)), "image/jpeg"));
////                    }
////                    dataParts.put(entry.getKey(), datas);
////                }
////                return dataParts;*/
////
////                return null;
////            }
////
////            @Override
////            protected Map<String, DataPart> getByteData() throws AuthFailureError {
////                Map<String, DataPart> params = new HashMap<>();
////                // file name could found file base or direct access from real path
////                // for now just get bitmap data from ImageView
////
//////                Map<String, List<DataPart>> dataParts = new HashMap<>();
////                for (Map.Entry<String, List<String>> entry : filepaths.entrySet()) {
//////                    List<DataPart> datas = new ArrayList<>();
////                    List<String> fileList = entry.getValue();
////                    for (int i = 0; i < fileList.size(); i++) {
////                        params.put("job_images[]", new DataPart(System.currentTimeMillis() + ".jpg", GlobalAccess.getByteDataFromFile(fileList.get(i)), "image/jpeg"));
//////                        datas.add(new DataPart("img_"+(i+1)+".jpg", GlobalAccess.getByteDataFromFile(fileList.get(i)), "image/jpeg"));
////                    }
//////                    dataParts.put(entry.getKey(), datas);
////                }
//////                if(){
//////                    params.put("profile_pic", new DataPart("profile_pic.jpg", GlobalAccess.getByteDataFromFile(filepath), "image/jpeg"));
//////                }else{
//////                    return null;
//////                    //params.put("profile_pic", new DataPart("profile_pic.jpg", GlobalAccess.getByteDataFromUrl(filepath), "image/jpeg"));
//////                }
////
////
////                return params;
////            }
////        };
////
////
////        VolleySingleton.getInstance(mActivity.getBaseContext()).addToRequestQueue(multipartRequest);
////        if (isDialog) {
////            showDialog("Please wait...");
////        }
////    }
//
//    private void showDialog(String message) {
////        dialog.setCancelable(false);
//        dialog.setCanceledOnTouchOutside(false);
//        dialog.setMessage(message);
//        dialog.show();
//    }
//
//    private void closeDialog() {
//        if (dialog != null && dialog.isShowing()) {
//            dialog.dismiss();
//        }
//    }
//}
