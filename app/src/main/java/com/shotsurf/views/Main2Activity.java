package com.shotsurf.views;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.shotsurf.HomeActivity;
import com.shotsurf.R;
import com.shotsurf.common.AppConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Main2Activity extends BaseActivity {
    String name, password, phoneNumber, username,email="";
    EditText userName;
    Button next;
    TextView userValidation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Bundle bundle = getIntent().getExtras();
        name = bundle.getString("Name");
        userValidation = (TextView) findViewById(R.id.user_validation);
//        phoneNumber = bundle.getString("Number");
        password = bundle.getString("Pass");
        Log.e("name", "" + name);
        Log.e("password", "" + password);
        if(CommonUtils.getPreferences(Main2Activity.this, AppConstants.KEY_PHONE)!=null)
        {
            phoneNumber=(CommonUtils.getPreferences(Main2Activity.this, AppConstants.KEY_PHONE));
        }

        if(CommonUtils.getPreferences(Main2Activity.this, AppConstants.KEY_EMAIL)!=null)
        {
            email=(CommonUtils.getPreferences(Main2Activity.this, AppConstants.KEY_EMAIL));
        }

        userName = (EditText) findViewById(R.id.username);
        next = (Button) findViewById(R.id.usernext);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userValidation.setVisibility(View.GONE);
                username = userName.getText().toString();
                if (TextUtils.isEmpty(username)) {
                    userValidation.setVisibility(View.VISIBLE);
                    userValidation.setText("Please enter valid username");
                } else if (username.length() < 2) {
                    userValidation.setVisibility(View.VISIBLE);
                    userValidation.setText("Atleast Two Characters");
                } else if (!username.matches("[a-zA-Z ]+")) {
                    userValidation.setVisibility(View.VISIBLE);
                    userValidation.setText("Enter only alphabet");
                } else {
                    new LoginTask().execute();
                }


            }
        });

    }

    class LoginTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            final OkHttpClient okHttpClient = new OkHttpClient();
            RequestBody formBody = new FormBody.Builder()
                    .add("password", password)
                    .add("action", "signUp")
                    .add("name", name)
                    .add("userName", username)
                    .add("phone",phoneNumber)
                    .add("email",email)
                    .build();
            Request request = new Request.Builder()
                    .url("http://www.404coders.com/shotsurf/services/services.php")
                    .post(formBody)
                    .build();

            try {
                Response response = okHttpClient.newCall(request).execute();
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                return response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress("");
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            hideProgress();
            Log.e("Response", " " + s);
            if (s != null) {

                try {


                    JSONObject jsonObject = new JSONObject(s);
                    String response = jsonObject.getString("responseMessage");
                    String responseCode = jsonObject.getString("responseCode");

                    toast(response);
                    if (response.equals("Sign up successfully.")) {
                        Intent intent = new Intent(Main2Activity.this, HomeActivity.class);
//                        Bundle bundle = new Bundle();
//                        bundle.putString("OTP",code);
//                        intent.putExtras(bundle);
                        startActivity(intent);


                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
            else
            {
                toast("Server not Responding.");
            }
        }
    }
}



