package com.shotsurf.views;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.shotsurf.R;

import java.util.ArrayList;

/**
 * Created by Admin on 10/6/2017.
 */

public class SyncNewActivity extends AppCompatActivity {

    RecyclerView RecyclerViewSync;
    SyncRecylerAdapter syncRecylerAdapter;
    ArrayList<SyncModel> syncList ;
    RelativeLayout linlayout;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.syncnew__main);

        RecyclerViewSync = (RecyclerView)findViewById(R.id.RecyclerViewSync);

RecyclerViewSync.setFocusable(false);

        RecyclerViewSync.setNestedScrollingEnabled(false);

        linlayout = (RelativeLayout) findViewById(R.id.relativelayout);

        syncnewlist();

        syncRecylerAdapter = new SyncRecylerAdapter(syncList, this);

        RecyclerViewSync.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));

        RecyclerViewSync.setAdapter(syncRecylerAdapter);


      /*  LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View snapshot = inflater.inflate(R.layout.listviewtab, null);

        linlayout.addView(snapshot);*/


        View child = getLayoutInflater().inflate(R.layout.listviewtab, null);


        linlayout.addView(child);


    }

    private void syncnewlist() {

        syncList = new ArrayList<>();

        for (int i = 0; i < 20; i++) {


            syncList.add(new SyncModel(R.drawable.i,"sheetal@404coders.com ","Follow"));


        }
    }
}
