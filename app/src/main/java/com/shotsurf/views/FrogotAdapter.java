package com.shotsurf.views;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.shotsurf.fragments.CameraFragment;
import com.shotsurf.fragments.EmailFrag;
import com.shotsurf.fragments.EmailFragment;
import com.shotsurf.fragments.NewFragment;
import com.shotsurf.fragments.PhoneFrag;

/**
 * Created by Admin on 10/5/2017.
 */

public class FrogotAdapter extends FragmentPagerAdapter {
    public FrogotAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment f= null;

        if(position==0)
        {
            f = new PhoneFrag();
        }
        else if(position==1)
        {
            f = new EmailFrag();
        }


        return f;
    }
    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        if(position==0)
        {
            return "Phone";
        }
        if (position==1)
        {
            return "Email";
        }
        return super.getPageTitle(position);
    }
}

