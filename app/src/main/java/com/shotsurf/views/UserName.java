package com.shotsurf.views;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.shotsurf.HomeActivity;
import com.shotsurf.R;
import com.shotsurf.common.AppConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class UserName extends BaseActivity{
EditText userName;
    TextView userNameValidation;
    Button next;
    String value;
    Context context;
    String user_id="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_name);
        context=UserName.this;
        user_id=CommonUtils.getPreferences(context,AppConstants.USER_ID);
        userName = (EditText)findViewById(R.id.username);
        userNameValidation=(TextView)findViewById(R.id.user_no_validity);
        next=(Button)findViewById(R.id.user_name_next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userNameValidation.setVisibility(View.GONE);
                value = userName.getText().toString();

                if(TextUtils.isEmpty(value))
                {
                    userNameValidation.setVisibility(View.VISIBLE);
                    userNameValidation.setText("Please enter username");
                }
                else if (!value.matches("[a-zA-Z ]+"))
                {
                    userNameValidation.setVisibility(View.VISIBLE);
                    userNameValidation.setText("Accepts only alphabet");
                }
                else
                {

                 startActivity(new Intent(UserName.this,HomeActivity.class));
                }
            }
        });
    }




    class UserNameApi extends AsyncTask<String,Void,String>
    {

        @Override
        protected String doInBackground(String... params) {
            final OkHttpClient okHttpClient = new OkHttpClient();
            RequestBody formBody = new FormBody.Builder()
                    .add("action", "userNameRegistred")
                    .add("userID", user_id)
                    .add("userName",value )
                    .build();
            Request request = new Request.Builder()
                    .url("http://www.404coders.com/shotsurf/services/services.php")
                    .post(formBody)
                    .build();
            try {
                Response response = okHttpClient.newCall(request).execute();
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                return response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(String servicesResponse) {
            super.onPostExecute(servicesResponse);
            try {

                /*{"responseCode":"000","responseMessage":"UserName already exits."}*/

                JSONObject jsonObject = new JSONObject(servicesResponse);
                String responseCode = jsonObject.getString("responseCode");
                String message = jsonObject.getString("responseMessage");
                String userName = jsonObject.getString("UserName");
                CommonUtils.savePreferencesString(context, AppConstants.USER_NAME, userName);

                Log.e("TAG","UserNameApi:::"+servicesResponse);

                if (!TextUtils.isEmpty(responseCode)) {

                    if (responseCode.equalsIgnoreCase("200")) {
                        toast(message);
                        startActivity(new Intent(UserName.this, HomeActivity.class));
                    }
                    else if (responseCode.equalsIgnoreCase("000")){
                        toast(message);
                       // startActivity(new Intent(UserName.this, HomeActivity.class));
                    }
                    else {
                        startActivity(new Intent(UserName.this, HomeActivity.class));
                    }
                }
                else {

                    Log.e("TAG","responsecode");
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
