package com.shotsurf.views;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.shotsurf.HomeActivity;
import com.shotsurf.R;
import com.shotsurf.RegistrationActivity;
import com.shotsurf.common.AppConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Main3Activity extends BaseActivity {
    String value;
    EditText otpText;
    Button confirm,resend;
    String newvalue,phone,email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        Bundle bundle = getIntent().getExtras();
        value = bundle.getString("OTP");
        resend=(Button)findViewById(R.id.resend);


        if(CommonUtils.getPreferences(Main3Activity.this, AppConstants.KEY_PHONE)!=null)
        {
            phone=(CommonUtils.getPreferences(Main3Activity.this, AppConstants.KEY_PHONE));
        }

        if(CommonUtils.getPreferences(Main3Activity.this, AppConstants.KEY_EMAIL)!=null)
        {
            email=(CommonUtils.getPreferences(Main3Activity.this, AppConstants.KEY_EMAIL));
        }


        Log.e("Value"," "+value);
        otpText = (EditText) findViewById(R.id.confirmation);
        otpText.setText(value);
        confirm = (Button) findViewById(R.id.next_confirm);

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newvalue = otpText.getText().toString();
                if (newvalue != null) {
                    new LoginTask().execute();
                }


            }
        });
        resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               new EmailOtp().execute();
            }
        });

    }

    class LoginTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            final OkHttpClient okHttpClient = new OkHttpClient();
            RequestBody formBody = new FormBody.Builder()
                    .add("action", "confirmation")
                    .add("code", newvalue)
                    .build();
            Request request = new Request.Builder()
                    .url("http://www.404coders.com/shotsurf/services/services.php")
                    .post(formBody)
                    .build();

            try {
                Response response = okHttpClient.newCall(request).execute();
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                return response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress("");

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            hideProgress();
            if (s != null) {
                try {


                    Log.e("value", " " + s);
                    JSONObject jsonObject = new JSONObject(s);
                    String response = jsonObject.getString("responseMessage");
                    toast(response);
                    if (response.equals("Code verify")) {

                        startActivity(new Intent(Main3Activity.this, MainActivity.class));
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else
            {
              toast("Server not Responding.");
            }
        }

    }
    class EmailOtp extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            final OkHttpClient okHttpClient = new OkHttpClient();
            RequestBody formBody = new FormBody.Builder()
                    .add("email", email)
                    .add("action", "mobileEmailverfi")
                    .add("phone",phone)
                    .build();
            Request request = new Request.Builder()
                    .url("http://www.404coders.com/shotsurf/services/services.php")
                    .post(formBody)
                    .build();
            try {
                Response response = okHttpClient.newCall(request).execute();
                if (!response.isSuccessful())
                    throw new IOException("Unexpected code " + response);
                return response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress("");
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            hideProgress();
            Log.e("Value", " " + s);
            if(s!=null)
                try {


                    JSONObject jsonObject = new JSONObject(s);
                    String response = jsonObject.getString("responseMessage");
                    String Code = jsonObject.getString("Code");
                    String responseCode = jsonObject.getString("responseCode");
                    Log.e("Error"," "+response);
                    toast(response);
                    if (responseCode.equals("200")) {

                       otpText.setText(Code);

                    }

//                else
//                {
//                    toast("User Already Exsit");
//                }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
        }
    }

}
