package com.shotsurf.views;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

/**
 * Created by Admin on 9/27/2017.
 */

public class BaseActivity extends AppCompatActivity {
    private ProgressDialog mDialog;

    //progress dialog
    //toast
    //snackbar
    // internet connectivity
    public void showProgress( String msg) {
        if (mDialog==null)
            mDialog = new ProgressDialog(BaseActivity.this);
        // mDialog.setTitle(title);
        mDialog.setMessage(msg);
        mDialog.setIndeterminate(true);
        mDialog.show();
    }

    public void hideProgress() {
        if (mDialog != null && mDialog.isShowing())
            mDialog.dismiss();
    }

    @Override
    protected void onStop() {
        super.onStop();
        hideProgress();
    }

    public void toast(String msg) {
        Toast.makeText(BaseActivity.this, msg, Toast.LENGTH_LONG).show();
    }

    public void snack(View v, String msg) {
        Snackbar.make(v, msg, Snackbar.LENGTH_LONG).show();
    }

    public boolean isInternetAvailable() {
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

}
