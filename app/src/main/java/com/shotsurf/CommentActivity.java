package com.shotsurf;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shotsurf.bean.PostData;
import com.shotsurf.fragments.FragmentNotification;

import java.util.ArrayList;
import java.util.List;

public class CommentActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private List<PostData> postDataList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);

        postDataList = new ArrayList<>();
        postDataList = getPostDataList();
        recyclerView = (RecyclerView) findViewById(R.id.commentList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        CommentAdapter ngoAdapter = new CommentAdapter();
        recyclerView.setAdapter(ngoAdapter);
        findViewById(R.id.backImage).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
    private List<PostData> getPostDataList() {
        List<PostData> postList = new ArrayList<>();
        PostData postData1 = new PostData();
        postData1.setUserName("kaira liked your comment: Nice look");
        postList.add(postData1);

        PostData postData2 = new PostData();
        postData2.setUserName("amit liked your comment: Nice look");
        postList.add(postData2);

        PostData postData3 = new PostData();
        postData3.setUserName("manoj commented: Out of focus image dcfhhf djdjd djdj");
        postList.add(postData3);


        return postList;
    }

    private class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.PostViewHolder> {

        @Override
        public int getItemCount() {
            return postDataList.size();
        }

        @Override
        public void onBindViewHolder(final CommentAdapter.PostViewHolder vh, final int i) {
            PostData data = postDataList.get(i);
            vh.userName.setText(data.getUserName());

        }

        @Override
        public CommentAdapter.PostViewHolder onCreateViewHolder(ViewGroup viewGroup, int arg1) {
            View itemView = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.comment_list_item, viewGroup, false);
            return new CommentAdapter.PostViewHolder(itemView);
        }

        public class PostViewHolder extends RecyclerView.ViewHolder {
            protected TextView userName,rvPostText;

            public PostViewHolder(View v) {
                super(v);
                userName = (TextView) v.findViewById(R.id.userName);
                rvPostText = (TextView) v.findViewById(R.id.rvPostText);
            }
        }
    }
}
