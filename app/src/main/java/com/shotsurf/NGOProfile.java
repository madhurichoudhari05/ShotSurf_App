package com.shotsurf;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

public class NGOProfile extends AppCompatActivity {
    private ArrayList<Integer> itemsimg;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ngoprofile);
        inflateXmlData();
    }

    private void inflateXmlData() {
        itemsimg = new ArrayList<>();
        itemsimg.add(R.drawable.img1);
        itemsimg.add(R.drawable.img2);
        itemsimg.add(R.drawable.img3);
        itemsimg.add(R.drawable.img1);
        itemsimg.add(R.drawable.img2);
        itemsimg.add(R.drawable.img3);
        itemsimg.add(R.drawable.img1);
        itemsimg.add(R.drawable.img2);
        itemsimg.add(R.drawable.img3);

        findViewById(R.id.txtContact).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), RedeemActivity.class));
            }
        });
        findViewById(R.id.txtDonate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), DonateActivity.class));
            }
        });
        findViewById(R.id.imgSetting).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), SettingActivity.class));
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        LinearLayoutManager recyclerViewLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(recyclerViewLayoutManager);
        profileAdapter profileAdapter = new profileAdapter();
        recyclerView.setAdapter(profileAdapter);

    }

    private class profileAdapter extends RecyclerView.Adapter<profileAdapter.PostViewHolder> {

        @Override
        public int getItemCount() {
            return itemsimg.size();
        }

        @Override
        public void onBindViewHolder(final profileAdapter.PostViewHolder vh, final int i) {
            vh.postMedia.setImageResource(itemsimg.get(i));

        }

        @Override
        public profileAdapter.PostViewHolder onCreateViewHolder(ViewGroup viewGroup, int arg1) {
            View itemView = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.ngo_profile_item, viewGroup, false);
            return new profileAdapter.PostViewHolder(itemView);
        }

        public class PostViewHolder extends RecyclerView.ViewHolder {
            protected ImageView postMedia;

            public PostViewHolder(View v) {
                super(v);
                postMedia = (ImageView) v.findViewById(R.id.image);
            }
        }
    }
}
