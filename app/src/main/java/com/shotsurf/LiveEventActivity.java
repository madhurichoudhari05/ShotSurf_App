package com.shotsurf;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.shotsurf.bean.Game;
import com.shotsurf.bean.PostData;

import java.util.ArrayList;
import java.util.List;

import it.moondroid.coverflow.components.ui.containers.FeatureCoverFlow;

public class LiveEventActivity extends AppCompatActivity {
    private FeatureCoverFlow coverFlow;
    private CoverFlowAdapter adapter;
    private ArrayList<Game> games;
    private RecyclerView recyclerView, eventRecyclerView;
    private List<PostData> postDataList;
    private ArrayList<Integer> itemsimg;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_event);

        coverFlow = (FeatureCoverFlow) findViewById(R.id.coverflow);
        postDataList = new ArrayList<>();
        postDataList = getPostDataList();

        /*Slide video view */
        settingDummyData();
        adapter = new CoverFlowAdapter(this, games);
        coverFlow.setAdapter(adapter);
        coverFlow.setOnScrollPositionListener(onScrollListener());
        inflateXmlData();

    }

    /*Demo image add*/
    private void inflateXmlData() {
        itemsimg = new ArrayList<Integer>();
        itemsimg.add(R.drawable.img1);
        itemsimg.add(R.drawable.img2);
        itemsimg.add(R.drawable.img3);
        itemsimg.add(R.drawable.img1);
        itemsimg.add(R.drawable.img2);
        itemsimg.add(R.drawable.img3);

          /* First event Recycler view here*/
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true));
        EventAdapter eventAdapter = new EventAdapter();
        recyclerView.setAdapter(eventAdapter);

        /*Second event Recycler view here*/
        eventRecyclerView = (RecyclerView) findViewById(R.id.recyclerView1);
        eventRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true));
        SecondEventAdapter SecondEventAdapter = new SecondEventAdapter();
        eventRecyclerView.setAdapter(SecondEventAdapter);

    }

    /*Demo data add in in modal class here*/
    private List<PostData> getPostDataList() {
        List<PostData> postList = new ArrayList<>();
        PostData postData1 = new PostData();
        postData1.setUserName("Karan");
        postList.add(postData1);

        PostData postData2 = new PostData();
        postData2.setUserName("Kaira");
        postList.add(postData2);

        PostData postData3 = new PostData();
        postData3.setUserName("Amit");
        postList.add(postData3);

        PostData postData4 = new PostData();
        postData4.setUserName("Raja");
        postList.add(postData4);


        PostData postData5 = new PostData();
        postData5.setUserName("ABC");
        postList.add(postData5);


        PostData postData6 = new PostData();
        postData6.setUserName("XYZ");
        postList.add(postData6);


        return postList;
    }

    private FeatureCoverFlow.OnScrollPositionListener onScrollListener() {
        return new FeatureCoverFlow.OnScrollPositionListener() {
            @Override
            public void onScrolledToPosition(int position) {
                Log.v("MainActiivty", "position: " + position);
            }

            @Override
            public void onScrolling() {

                Log.i("MainActivity", "scrolling");
            }
        };
    }

    private void settingDummyData() {
        games = new ArrayList<>();
        games.add(new Game(R.drawable.img3, "video1"));
        games.add(new Game(R.drawable.img2, "Video2"));
        games.add(new Game(R.drawable.img1, "Video3"));
    }
    public class CoverFlowAdapter extends BaseAdapter {


        private ArrayList<Game> data;
        private AppCompatActivity activity;

        public CoverFlowAdapter(AppCompatActivity context, ArrayList<Game> objects) {
            this.activity = context;
            this.data = objects;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Game getItem(int position) {
            return data.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder viewHolder;

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.item_flow_view, null, false);

                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.gameImage.setImageResource(data.get(position).getImageSource());
//            viewHolder.gameName.setText(data.get(position).getName());

            convertView.setOnClickListener(onClickListener(position));

            return convertView;
        }

        private View.OnClickListener onClickListener(final int position) {
            return new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    final Dialog dialog = new Dialog(activity);
                    dialog.setContentView(R.layout.dialog_game_info);
                    dialog.setCancelable(true); // dimiss when touching outside
                    dialog.setTitle("Game Details");

                    TextView text = (TextView) dialog.findViewById(R.id.name);
                    text.setText(getItem(position).getName());
                    ImageView image = (ImageView) dialog.findViewById(R.id.image);
                    image.setImageResource(getItem(position).getImageSource());

                    dialog.show();
                }
            };
        }


        private class ViewHolder {
            private TextView gameName;
            private ImageView gameImage;

            public ViewHolder(View v) {
                gameImage = (ImageView) v.findViewById(R.id.image);
                gameName = (TextView) v.findViewById(R.id.name);
            }
        }
    }

    private class EventAdapter extends RecyclerView.Adapter<EventAdapter.PostViewHolder> {

        @Override
        public int getItemCount() {
            return postDataList.size();
        }

        @Override
        public void onBindViewHolder(final EventAdapter.PostViewHolder vh, final int i) {
            final PostData data = postDataList.get(i);
            vh.postMedia.setImageResource(itemsimg.get(i));

        }

        @Override
        public EventAdapter.PostViewHolder onCreateViewHolder(ViewGroup viewGroup, int arg1) {
            View itemView = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.live_event_item, viewGroup, false);
            return new EventAdapter.PostViewHolder(itemView);
        }

        public class PostViewHolder extends RecyclerView.ViewHolder {
            protected ImageView postMedia;

            public PostViewHolder(View v) {
                super(v);
                postMedia = (ImageView) v.findViewById(R.id.postMedia);
            }
        }
    }

    private class SecondEventAdapter extends RecyclerView.Adapter<SecondEventAdapter.PostViewHolder> {

        @Override
        public int getItemCount() {
            return postDataList.size();
        }

        @Override
        public void onBindViewHolder(final SecondEventAdapter.PostViewHolder vh, final int i) {
            final PostData data = postDataList.get(i);
            vh.postMedia.setImageResource(itemsimg.get(i));

        }

        @Override
        public SecondEventAdapter.PostViewHolder onCreateViewHolder(ViewGroup viewGroup, int arg1) {
            View itemView = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.live_event_item, viewGroup, false);
            return new SecondEventAdapter.PostViewHolder(itemView);
        }

        public class PostViewHolder extends RecyclerView.ViewHolder {
            protected ImageView postMedia;

            public PostViewHolder(View v) {
                super(v);
                postMedia = (ImageView) v.findViewById(R.id.postMedia);
            }
        }
    }
}
