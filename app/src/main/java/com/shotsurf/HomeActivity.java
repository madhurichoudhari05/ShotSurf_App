package com.shotsurf;






import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.shotsurf.common.AppConstants;
import com.shotsurf.fragments.FragmentAdapter;
import com.shotsurf.fragments.FragmentHome;
import com.shotsurf.fragments.FragmentNotification;
import com.shotsurf.fragments.FragmentProfile;
import com.shotsurf.fragments.FragmentSearch;
import com.shotsurf.views.CommonUtils;


public class HomeActivity extends AppCompatActivity {
    ViewPager viewPager;
    FragmentAdapter fragmentAdapter;
    private Fragment fragment;
    int value=1;
    Context mContext;
    private static final int MY_CAMERA_REQUEST_CODE = 100;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        mContext=HomeActivity.this;


        CommonUtils.savePreferencesBoolean(mContext, AppConstants.FIRST_TIME_TUTORIAL, true);
//        viewPager=(ViewPager)findViewById(R.id.viewPager);
//
//
//       /* fragment = new FragmentHome();
//        CommonUtils.setFragment(fragment, false, this, R.id.viewPager);
//*/
////        CommonUtils.setFragment(viewPager);
//
//
//
//        fragmentAdapter = new FragmentAdapter(getSupportFragmentManager());
//        viewPager.setAdapter(fragmentAdapter);
//        viewPager.setCurrentItem(2);
//
       fragment = new FragmentHome();
        setFragment();
        inflateXmlData();
    }

    private void inflateXmlData() {
        findViewById(R.id.layoutHome).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findViewById(R.id.imgHome).setBackgroundResource(R.drawable.ic_home_blue);
                findViewById(R.id.imgSearch).setBackgroundResource(R.drawable.ic_search_gray);
                findViewById(R.id.imgPic).setBackgroundResource(R.drawable.ic_add_box);
                findViewById(R.id.imgNotification).setBackgroundResource(R.drawable.ic_notifications_gray);
                findViewById(R.id.imgProfile).setBackgroundResource(R.drawable.ic_person_gray);
                fragment = new FragmentHome();
                setFragment();
            }
        });
        findViewById(R.id.layoutSearch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findViewById(R.id.imgHome).setBackgroundResource(R.drawable.ic_home_gray);
                findViewById(R.id.imgSearch).setBackgroundResource(R.drawable.ic_search_blue);
                findViewById(R.id.imgPic).setBackgroundResource(R.drawable.ic_add_box);
                findViewById(R.id.imgNotification).setBackgroundResource(R.drawable.ic_notifications_gray);
                findViewById(R.id.imgProfile).setBackgroundResource(R.drawable.ic_person_gray);
                fragment = new FragmentSearch();
                setFragment();
            }
        });
        findViewById(R.id.layoutUpload).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*findViewById(R.id.imgHome).setBackgroundResource(R.drawable.ic_home_gray);
                findViewById(R.id.imgSearch).setBackgroundResource(R.drawable.ic_search_gray);
                findViewById(R.id.imgPic).setBackgroundResource(R.drawable.ic_upload_blue);
                findViewById(R.id.imgNotification).setBackgroundResource(R.drawable.ic_notifications_gray);
                findViewById(R.id.imgProfile).setBackgroundResource(R.drawable.ic_person_gray);
                fragment = new FragmentFileUpload();
                setFragment();*/
                startActivity(new Intent(getApplicationContext(), UploadeFileActivity.class)); 
            }
        });
        findViewById(R.id.layoutNotification).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findViewById(R.id.imgHome).setBackgroundResource(R.drawable.ic_home_gray);
                findViewById(R.id.imgSearch).setBackgroundResource(R.drawable.ic_search_gray);
                findViewById(R.id.imgPic).setBackgroundResource(R.drawable.ic_add_box);
                findViewById(R.id.imgNotification).setBackgroundResource(R.drawable.ic_notifications_blue);
                findViewById(R.id.imgProfile).setBackgroundResource(R.drawable.ic_person_gray);
                fragment = new FragmentNotification();
                setFragment();
            }
        });

        findViewById(R.id.layoutProfile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findViewById(R.id.imgHome).setBackgroundResource(R.drawable.ic_home_gray);
                findViewById(R.id.imgSearch).setBackgroundResource(R.drawable.ic_search_gray);
                findViewById(R.id.imgPic).setBackgroundResource(R.drawable.ic_add_box);
                findViewById(R.id.imgNotification).setBackgroundResource(R.drawable.ic_notifications_gray);
                findViewById(R.id.imgProfile).setBackgroundResource(R.drawable.ic_person_blue);
                fragment= new FragmentProfile();
                setFragment();
            }
        });
    }
    private void setFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, fragment );
        fragmentTransaction.commit();
//        fragment = new FragmentProfile();

    }
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        value++;
        if(value==3)
        {
            finishAffinity();
        }
        else {
            //Toast.makeText(this, "back", Toast.LENGTH_SHORT).show();
           // super.onBackPressed();
        }
    }
}